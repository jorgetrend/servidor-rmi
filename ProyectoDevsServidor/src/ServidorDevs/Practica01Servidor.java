package ServidorDevs;

import Interfaces.IPedidoController;
import Interfaces.IProductoController;
import Interfaces.IVentaController;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Practica01Servidor {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);
            
            IProductoController productoController = new ProductoController();
            Naming.rebind("rmi://localhost/ProductoController", productoController);
            
            IPedidoController pedidoController = new PedidoController();
            Naming.rebind("rmi://localhost/PedidoController", pedidoController);
            
            IVentaController ventaController = new VentaController();
            Naming.rebind("rmi://localhost/VentaController", ventaController);
            
            System.out.println("Escuchando....");
           
        } catch (RemoteException ex) {
            Logger.getLogger(Practica01Servidor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "No se puede conectar con el servidor"
                    ,"Error de conexión",JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Practica01Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}
