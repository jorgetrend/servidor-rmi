package ServidorDevs;

import Interfaces.IVenta;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Venta extends UnicastRemoteObject implements IVenta {

    private int id_venta;
    private int id_pedido;
    private String estado_venta;
    private double monto_venta;
    private String nombre_cliente;
    private Date fecha_venta;

    public Venta() throws RemoteException {

    }

    public Venta(int id_venta, String estado_venta, double monto_venta, Date fecha_venta) throws RemoteException {
        this.id_venta = id_venta;
        this.estado_venta = estado_venta;
        this.monto_venta = monto_venta;
        this.fecha_venta = fecha_venta;
    }

    @Override
    public int getId_venta() throws RemoteException {
        return id_venta;
    }

    @Override
    public void setId_venta(int id_venta) throws RemoteException {
        this.id_venta = id_venta;
    }

    @Override
    public String getEstado_venta() throws RemoteException {
        return estado_venta;
    }

    @Override
    public void setEstado_venta(String estado_venta) throws RemoteException {
        this.estado_venta = estado_venta;
    }

    @Override
    public double getMonto_venta() throws RemoteException {
        return monto_venta;
    }

    @Override
    public void setMonto_venta(double monto_venta) throws RemoteException {
        this.monto_venta = monto_venta;
    }

    @Override
    public Date getFecha_venta() throws RemoteException {
        return fecha_venta;
    }

    @Override
    public void setFecha_venta(Date fecha_venta) throws RemoteException {
        this.fecha_venta = fecha_venta;
    }

    @Override
    public String getString() throws RemoteException {
        return String.format("Id Venta: %d, "
                + "Id Pedido: %d,"
                + " Estado Venta: %s,"
                + " Monto Venta: %f,"
                + " Nombre Cliente: %s,"
                + " Fecha Venta: %tD",
                id_venta,
                id_pedido,
                estado_venta,
                monto_venta,
                nombre_cliente,
                fecha_venta);
    }

    public static IVenta fromMap(Map<String, Object> map) throws RemoteException {
        IVenta venta = new Venta();

        if (map.containsKey("id_venta")) {
            venta.setId_venta((Integer) map.get("id_venta"));
        }

        if (map.containsKey("estado_venta")) {
            venta.setEstado_venta((String) map.get("estado_venta"));
        }

        if (map.containsKey("monto_venta")) {
            venta.setMonto_venta((double) map.get("monto_venta"));
        }

//        if (map.containsKey("fecha_venta")) {
//            venta.setFecha_venta((Date) map.get("fecha_venta"));
//        }

        return venta;
    }

    public static Map<String, Object> toMap(IVenta venta) throws RemoteException {
        Map<String, Object> datos = new HashMap<>();

        if (venta.getId_venta() != 0) {
            datos.put("id_venta", venta.getId_venta());
        }

        if (venta.getEstado_venta() != null) {
            datos.put("estado_venta", venta.getEstado_venta());
        }

        if (venta.getMonto_venta() != 0) {
            datos.put("monto_venta", venta.getMonto_venta());
        }

        if (venta.getFecha_venta() != null) {
            datos.put("fecha_venta", venta.getFecha_venta());
        }

        return datos;
    }

}
