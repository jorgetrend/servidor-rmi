package ServidorDevs;

import Interfaces.IProducto;
import Interfaces.IProductoController;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductoController extends UnicastRemoteObject implements IProductoController {

    private DBManager dbManager;
    private final String TABLE = "Producto";

    public ProductoController() throws RemoteException {
        dbManager = new DBManager();
    }

    @Override
    public IProducto newInstance() throws RemoteException {
        return new Producto();
    }

    @Override
    public int add(IProducto producto) throws RemoteException {

        IProducto productoEncontrado = findOne(producto.getId_producto());

        boolean existe = productoEncontrado.getId_producto() != 0;

        if (existe) {
            return ADD_ID_DUPLICADO;
        }

        Map<String, Object> datos = Producto.toMap(producto);
        int respuesta = dbManager.insertar(TABLE, datos);

        return (respuesta > 0) ? ADD_EXITO : ADD_SIN_EXITO;
    }

    @Override
    public int update(IProducto producto) throws RemoteException {
        if (producto.getId_producto() == 0) {
            return UPDATE_ID_NULO;
        }
        //Verificar producto existente con ID recibido
        IProducto productoEncontrado = findOne(producto.getId_producto());
        if (productoEncontrado.getId_producto() == 0) {
            return UPDATE_ID_INEXISTENTE;
        }

        Map<String, Object> datos = Producto.toMap(producto);
        Map<String, Object> where = new HashMap<>();
        where.put("id_producto", producto.getId_producto());
        int respuesta = dbManager.actualizar(TABLE, datos, where);
        return (respuesta > 0) ? UPDATE_EXITO : UPDATE_SIN_EXITO;

    }

    @Override
    public int delete(IProducto producto) throws RemoteException {
        IProducto productoTemp = findOne(producto.getId_producto());
        if (productoTemp.getId_producto() == 0) {
            return DELETE_ID_INEXISTENTE;
        }

        Map<String, Object> where = new HashMap<>();
        where.put("id_producto", producto.getId_producto());
        int respuesta = dbManager.eliminar(TABLE, where);
        return (respuesta == 0) ? DELETE_SIN_EXITO : DELETE_EXITO;

    }

    @Override
    public int delete(int idProducto) throws RemoteException {
        IProducto producto = new Producto();
        producto.setId_producto(idProducto);
        return delete(producto);
    }

    @Override
    public IProducto findOne(int idProducto) throws RemoteException {
        Map<String, Object> where = new HashMap<>();
        where.put("id_producto", idProducto);
        Map<String, Object> registro = dbManager.buscarUno(TABLE, where);

        return Producto.fromMap(registro);
    }

    @Override
    public List<IProducto> find(IProducto producto) throws RemoteException {
        List<IProducto> listaProducto = new ArrayList<>();

        Map<String, Object> where = Producto.toMap(producto);
        List<Map<String, Object>> registros = dbManager.listar(TABLE, where);

        for (Map<String, Object> registro : registros) {
            IProducto productoTemp = Producto.fromMap(registro);
            listaProducto.add(productoTemp);
        }
        return listaProducto;
    }

    @Override
    public List<IProducto> list() throws RemoteException {
        List<IProducto> listaProducto = new ArrayList<>();
        List<Map<String, Object>> registros = dbManager.listar(TABLE);

        for (Map<String, Object> registro : registros) {
            IProducto producto = Producto.fromMap(registro);
            listaProducto.add(producto);
        }
        return listaProducto;
    }

}
