package ServidorDevs;

import Interfaces.IPedido;
import Interfaces.IPedidoController;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PedidoController extends UnicastRemoteObject implements IPedidoController {

    private DBManager dbManager;
    private final String TABLE = "Pedido";

    public PedidoController() throws RemoteException {
        dbManager = new DBManager();
    }

    @Override
    public IPedido newInstance() throws RemoteException {
        return new Pedido();
    }

    @Override
    public int add(IPedido pedido) throws RemoteException {
        IPedido pedidoEncontrado = findOne(pedido.getId_pedido());
        boolean existe = pedidoEncontrado.getId_pedido() != 0;

        if (existe) {
            return ADD_ID_DUPLICADO;
        }

        Map<String, Object> datos = Pedido.toMap(pedido);
        int respuesta = dbManager.insertar(TABLE, datos);

        return (respuesta > 0) ? ADD_EXITO : ADD_SIN_EXITO;
    }

    @Override
    public int update(IPedido pedido) throws RemoteException {
        if (pedido.getId_pedido() == 0) {
            return UPDATE_ID_NULO;
        }
        //Verificar persona existente con ID recibido
        IPedido pedidoEncontrado = findOne(pedido.getId_pedido());
        if (pedidoEncontrado.getId_pedido() == 0) {
            return UPDATE_ID_INEXISTENTE;
        }

        Map<String, Object> datos = Pedido.toMap(pedido);
        Map<String, Object> where = new HashMap<>();
        where.put("id_pedido", pedido.getId_pedido());
        int respuesta = dbManager.actualizar(TABLE, datos, where);

        return (respuesta > 0) ? ADD_EXITO : ADD_SIN_EXITO;
    }

    @Override
    public int delete(IPedido pedido) throws RemoteException {
        IPedido pedidoTemp = findOne(pedido.getId_pedido());

        if (pedidoTemp.getId_pedido() == 0) {
            return DELETE_ID_INEXISTENTE;
        }

        Map<String, Object> where = new HashMap<>();
        where.put("id_pedido", pedido.getId_pedido());
        int respuesta = dbManager.eliminar(TABLE, where);

        return (respuesta == 0) ? DELETE_SIN_EXITO : DELETE_EXITO;

    }

    @Override
    public int delete(int id_pedido) throws RemoteException {
        IPedido pedido = new Pedido();
        pedido.setId_pedido(id_pedido);
        return delete(pedido);
    }

    @Override
    public IPedido findOne(int id_pedido) throws RemoteException {
        Map<String, Object> where = new HashMap<>();
        where.put("id_pedido", id_pedido);
        Map<String, Object> registro = dbManager.buscarUno(TABLE, where);

        return Pedido.fromMap(registro);
    }

    @Override
    public List<IPedido> find(IPedido pedido) throws RemoteException {
        List<IPedido> listaPedido = new ArrayList<>();

        Map<String, Object> where = Pedido.toMap(pedido);
        List<Map<String, Object>> registros = dbManager.listar(TABLE, where);

        for (Map<String, Object> registro : registros) {
            IPedido pedidoTemp = Pedido.fromMap(registro);
            listaPedido.add(pedidoTemp);
        }
        return listaPedido;
    }

    @Override
    public List<IPedido> list() throws RemoteException {
        List<IPedido> listaPedido = new ArrayList<>();

        List<Map<String, Object>> registros = dbManager.listar(TABLE);

        for (Map<String, Object> registro : registros) {
            IPedido pedido = Pedido.fromMap(registro);
            listaPedido.add(pedido);
        }
        return listaPedido;
    }

    @Override
    public int findMax() throws RemoteException {
        int resultado = dbManager.buscarMaximo();
        return resultado;
    }

}
