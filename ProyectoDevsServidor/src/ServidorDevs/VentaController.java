package ServidorDevs;

import Interfaces.IVenta;
import Interfaces.IVentaController;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jose Luis
 */
public class VentaController extends UnicastRemoteObject implements IVentaController {

    private DBManager dbManager;
    private final String TABLE = "venta";

    public VentaController() throws RemoteException {
        dbManager = new DBManager();
    }

    @Override
    public IVenta newInstance() throws RemoteException {
        return new Venta();
    }

    @Override
    public int add(IVenta venta) throws RemoteException {
        IVenta ventaEncontrada = findOne(venta.getId_venta());
        boolean existe = ventaEncontrada.getId_venta() != 0;

        if (existe) {
            return ADD_ID_DUPLICADO;
        }
        Map<String, Object> datos = Venta.toMap(venta);
        int respuesta = dbManager.insertar(TABLE, datos);

        return (respuesta > 0) ? ADD_EXITO : ADD_SIN_EXITO;
    }

    @Override
    public int update(IVenta venta) throws RemoteException {
        if (venta.getId_venta() == 0) {
            return UPDATE_ID_NULO;
        }
        //Verificar venta existente con ID recibido
        IVenta ventaEncontrada = findOne(venta.getId_venta());
        if (ventaEncontrada.getId_venta() == 0) {
            return UPDATE_ID_INEXISTENTE;
        }

        Map<String, Object> datos = Venta.toMap(venta);
        Map<String, Object> where = new HashMap<>();
        where.put("id_venta", venta.getId_venta());
        int respuesta = dbManager.actualizar(TABLE, datos, where);

        return (respuesta > 0) ? UPDATE_EXITO : UPDATE_SIN_EXITO;

    }

    @Override
    public int delete(IVenta venta) throws RemoteException {
        IVenta ventaTemp = findOne(venta.getId_venta());
        if (ventaTemp.getId_venta() == 0) {
            return DELETE_ID_INEXISTENTE;
        }

        Map<String, Object> where = new HashMap<>();
        where.put("id_venta", venta.getId_venta());
        int respuesta = dbManager.eliminar(TABLE, where);

        return (respuesta == 0) ? DELETE_SIN_EXITO : DELETE_EXITO;

    }

    @Override
    public int delete(int idVenta) throws RemoteException {
        IVenta venta = new Venta();
        venta.setId_venta(idVenta);
        return delete(venta);
    }

    @Override
    public IVenta findOne(int idVenta) throws RemoteException {
        Map<String, Object> where = new HashMap<>();
        where.put("id_venta", idVenta);
        Map<String, Object> registro = dbManager.buscarUno(TABLE, where);

        return Venta.fromMap(registro);
    }

    @Override
    public List<IVenta> list() throws RemoteException {
        List<IVenta> listaVenta = new ArrayList<>();

        List<Map<String, Object>> registros = dbManager.listar(TABLE);

        for (Map<String, Object> registro : registros) {
            IVenta venta = Venta.fromMap(registro);
            listaVenta.add(venta);
        }
        return listaVenta;
    }

    @Override
    public List<IVenta> find(IVenta venta) throws RemoteException {
        List<IVenta> listaPersona = new ArrayList<>();

        Map<String, Object> where = Venta.toMap(venta);
        List<Map<String, Object>> registros = dbManager.listar(TABLE, where);

        for (Map<String, Object> registro : registros) {
            IVenta ventaTemp = Venta.fromMap(registro);
            listaPersona.add(ventaTemp);
        }
        return listaPersona;
    }

    @Override
    public List<String> getTime() throws RemoteException {
        List<String> resultado = dbManager.getTime();
        return resultado;

    }

    @Override
    public List<String> getTime(String where) throws RemoteException {
        List<String> resultado = dbManager.getTime(where);
        return resultado;
    }

    @Override
    public List<IVenta> listarFecha(String fechaInicio, String fechaFinal) throws RemoteException {
        List<Map<String, Object>> lista = dbManager.listarFecha(fechaInicio, fechaFinal);
        List<IVenta> listaPersona = new ArrayList<>();

        for (Map<String, Object> registro : lista) {
            IVenta ventaTemp = Venta.fromMap(registro);
            listaPersona.add(ventaTemp);
        }
        return listaPersona;
    }

}
