package ServidorDevs;

import Interfaces.IProducto;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class Producto extends UnicastRemoteObject implements IProducto {

    private int id_producto;
    private String nombre_producto;
    private String codigo_producto;
    private String descripcion_producto;
    private double precio_producto;

    public Producto() throws RemoteException {

    }

    public Producto(int id_producto, String nombre_producto, String codigo_producto, String descripcion_producto, double precio_producto) throws RemoteException {
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.codigo_producto = codigo_producto;
        this.descripcion_producto = descripcion_producto;
        this.precio_producto = precio_producto;
    }

    @Override
    public int getId_producto() {
        return id_producto;
    }

    @Override
    public void setId_producto(int id_producto) throws RemoteException {
        this.id_producto = id_producto;
    }

    @Override
    public String getNombre_producto() {
        return nombre_producto;
    }

    @Override
    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getCodigo_producto() {
        return codigo_producto;
    }

    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    @Override
    public String getDescripcion_producto() {
        return descripcion_producto;
    }

    @Override
    public void setDescripcion_producto(String descripcion_producto) {
        this.descripcion_producto = descripcion_producto;
    }

    @Override
    public double getPrecio_producto() {
        return precio_producto;
    }

    @Override
    public void setPrecio_producto(double precio_producto) {
        this.precio_producto = precio_producto;
    }

    @Override
    public String getString() throws RemoteException {
        return String.format("Id Producto: %d, "
                + "Nombre Producto: %s, "
                + "Código Producto: %s, "
                + "Descripción Producto: %s,"
                + " Precio Producto%f",
                id_producto,
                nombre_producto,
                codigo_producto,
                descripcion_producto,
                precio_producto);
    }

    public static IProducto fromMap(Map<String, Object> map) throws RemoteException {

        IProducto producto = new Producto();

        if (map.containsKey("id_producto")) {
            producto.setId_producto((Integer) map.get("id_producto"));
        }

        if (map.containsKey("nombre_producto")) {
            producto.setNombre_producto((String) map.get("nombre_producto"));
        }

        if (map.containsKey("codigo_producto")) {
            producto.setCodigo_producto("" + map.get("codigo_producto"));
        }

        if (map.containsKey("descripcion_producto")) {
            producto.setDescripcion_producto((String) map.get("descripcion_producto"));
        }

        if (map.containsKey("precio_producto")) {
            producto.setPrecio_producto((double) map.get("precio_producto"));
        }

        return producto;
    }

    public static Map<String, Object> toMap(IProducto producto) throws RemoteException {
        Map<String, Object> datos = new HashMap<>();
        //if(producto.getId_producto() != 0 )
        //    datos.put("id_producto", producto.getId_producto());

        if (producto.getNombre_producto() != null) {
            datos.put("nombre_producto", producto.getNombre_producto());
        }

        if (producto.getCodigo_producto() != null) {
            datos.put("codigo_producto", producto.getCodigo_producto());
        }

        if (producto.getDescripcion_producto() != null) {
            datos.put("descripcion_producto", producto.getDescripcion_producto());
        }

        if (producto.getPrecio_producto() != 0) {
            datos.put("precio_producto", producto.getPrecio_producto());
        }

        return datos;
    }

}
