package ServidorDevs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBManager {

    private Connection conexion = conexion();
    private DBManager dbManager;

    public DBManager() {
    } // Fin constructor

    public Connection conexion() {
        try {
            if (conexion == null) {
                Class.forName("com.mysql.jdbc.Driver");
                String usuario = "root"; //Crear un usuario especial que NO SEA ROOT
                String password = "";
                conexion = DriverManager.getConnection("jdbc:mysql://localhost/marketdevs", usuario, password);
                System.out.println("Conexión a BD exitosa.");
            } else {
                return conexion;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conexion;
    }

    /* Insertar */
    public int insertar(String tabla, Map< String, Object> datos) {
        String sql = String.format("INSERT INTO %s (", tabla);
        for (String columna : datos.keySet()) {
            sql += String.format("%s, ", columna);
        } /// Fin for

        sql = sql.substring(0, sql.length() - 2);
        sql += ") VALUES ( ";

        for (String columna : datos.keySet()) {
            Object dato = datos.get(columna);
            if (dato instanceof Number || dato instanceof Boolean) {
                sql += dato + ", ";
            } else {
                sql += String.format("'%s', ", dato);
            }
        } // Fin for
        sql = sql.substring(0, sql.length() - 2);
        sql += ");";

        System.out.println(sql);

        int respuesta = 0;
        try {
            Statement statement = conexion.createStatement();
            respuesta = statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return respuesta;
        }
    }

    /* Actualizar */
    public int actualizar(String tabla, Map<String, Object> datos) {
        return actualizar(tabla, datos, null);
    }

    public int actualizar(String tabla, Map<String, Object> datos, Map<String, Object> where) {
        String sql = String.format("UPDATE %s SET ", tabla);
        for (String columna : datos.keySet()) {
            Object dato = datos.get(columna);
            if (dato instanceof Number || dato instanceof Boolean) {
                sql += String.format("%s = %s, ", columna, dato);
            } else {
                sql += String.format("%s = '%s', ", columna, dato);
            }
        } /// Fin for

        sql = sql.substring(0, sql.length() - 2);

        if (where != null && !where.isEmpty()) {
            sql += " WHERE ";
            for (String columna : where.keySet()) {
                Object dato = where.get(columna);
                if (dato instanceof Number || dato instanceof Boolean) {
                    sql += String.format("%s = %s AND ", columna, dato);
                } else {
                    sql += String.format("%s = '%s' AND ", columna, dato);
                }
            } /// Fin for
            sql = sql.substring(0, sql.length() - 4);
        } // Fin if

        System.out.printf("Ejecutando: %s\n", sql);

        Statement statement;
        int respuesta = 0;
        try {
            statement = conexion.createStatement();
            respuesta = statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return respuesta;
        }
    } // Fin actualizar

    /* Eliminar */
    public int eliminar(String tabla) {
        return eliminar(tabla, null);
    }

    public int eliminar(String tabla, Map<String, Object> where) {
        String sql = String.format("DELETE FROM %s", tabla);

        if (where != null && !where.isEmpty()) {
            sql += " WHERE ";
            for (String columna : where.keySet()) {
                Object dato = where.get(columna);
                if (dato instanceof Number || dato instanceof Boolean) {
                    sql += String.format("%s = %s AND ", columna, dato);
                } else {
                    sql += String.format("%s = '%s' AND ", columna, dato);
                }
            } /// Fin for
            sql = sql.substring(0, sql.length() - 4);
        } // Fin if

        System.out.printf("Ejecutando: %s\n", sql);

        int respuesta = 0;
        try {
            Statement statement = conexion.createStatement();
            respuesta = statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return respuesta;
        }
    }

    /* LISTAR */
    public List< Map<String, Object>> listar(String tabla) {
        return listar(tabla, null);
    }

    public List< Map<String, Object>> listar(String tabla, Map<String, Object> where) {
        String sql = String.format("SELECT * FROM %s", tabla);

        if (where != null && !where.isEmpty()) {
            sql += " WHERE ";
            for (String columna : where.keySet()) {
                Object dato = where.get(columna);
                if (dato instanceof Number || dato instanceof Boolean) {
                    sql += String.format("%s = %s AND ", columna, dato);
                } else {
                    sql += String.format("%s = '%s' AND ", columna, dato);
                }
            } /// Fin for
            sql = sql.substring(0, sql.length() - 4);
        } // Fin if

        System.out.printf("Consultando: %s\n", sql);

        List< Map<String, Object>> registros = new ArrayList<>();
        try {
            Statement statement = conexion.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            ResultSetMetaData metaData = resultSet.getMetaData();
            final int TOTAL_COLUMNAS = metaData.getColumnCount();

            // Recuperar registros
            while (resultSet.next()) {
                Map<String, Object> registro = new HashMap<>();
                for (int i = 1; i <= TOTAL_COLUMNAS; i++) {
                    registro.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                registros.add(registro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return registros;
        }
    } // Fin listar(String,Map)

    /* BUSCAR UNO */
    public Map<String, Object> buscarUno(String tabla, Map<String, Object> where) {
        String sql = String.format("SELECT * FROM %s", tabla);

        if (where != null && !where.isEmpty()) {
            sql += " WHERE ";
            for (String columna : where.keySet()) {
                Object dato = where.get(columna);
                if (dato instanceof Number || dato instanceof Boolean) {
                    sql += String.format("%s = %s AND ", columna, dato);
                } else {
                    sql += String.format("%s = '%s' AND ", columna, dato);
                }
            } /// Fin for
            sql = sql.substring(0, sql.length() - 4);
        } // Fin if

        System.out.printf("Consultando: %s\n", sql);

        Map<String, Object> registro = new HashMap<>();
        try {
            Statement statement = conexion.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            ResultSetMetaData metaData = resultSet.getMetaData();
            final int TOTAL_COLUMNAS = metaData.getColumnCount();

            // Recuperar registros
            if (resultSet.next()) {
                for (int i = 1; i <= TOTAL_COLUMNAS; i++) {
                    registro.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return registro;
        }
    } // Fin listar(String,Map)

    public int buscarMaximo() {
        String sql = String.format("SELECT MAX(id_Venta) AS 'venta' FROM venta");
        System.out.printf("Consultando: %s\n", sql);
        int respuesta = 2;
        try {
            Statement statement = conexion.createStatement();
            ResultSet resultado = statement.executeQuery(sql);
            if (resultado.next()) {
                respuesta = resultado.getInt(1);
            }
            System.err.println(respuesta);
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return respuesta;
        }

    } // Fin buscarMaximo

    public List<String> getTime() {
        String tabla = "venta";
        String sql = String.format("SELECT fecha_venta FROM %s", tabla);

        System.err.printf("Consultando: %s\n", sql);

        List<String> registros = new ArrayList<>();

        try {
            Statement statement = conexion.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            // Recuperar registros
            while (resultSet.next()) {
                registros.add("" + resultSet.getTimestamp(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return registros;
        }
    }

    public List<String> getTime(String where) {
        String tabla = "venta";
        String sql = String.format("SELECT fecha_venta FROM %s WHERE %s", tabla, where);

        System.err.printf("Consultando: %s\n", sql);

        List<String> registros = new ArrayList<>();

        try {
            Statement statement = conexion.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            // Recuperar registros
            while (resultSet.next()) {
                registros.add("" + resultSet.getTimestamp(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return registros;
        }
    }

    public List<Map<String, Object>> listarFecha(String fechaInicio, String fechaFinal) {
        String sql = String.format("SELECT * FROM venta WHERE fecha_venta BETWEEN '%s 00:00:00' AND '%s 23:59:59'", fechaInicio, fechaFinal);

        System.err.printf("Consultando: %s\n", sql);

        List< Map<String, Object>> registros = new ArrayList<>();
        try {
            Statement statement = conexion.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            ResultSetMetaData metaData = resultSet.getMetaData();
            final int TOTAL_COLUMNAS = metaData.getColumnCount();

            // Recuperar registros
            while (resultSet.next()) {
                Map<String, Object> registro = new HashMap<>();
                for (int i = 1; i <= TOTAL_COLUMNAS; i++) {
                    registro.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                registros.add(registro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return registros;
        }
    } // Fin listar(String,Map)

}
