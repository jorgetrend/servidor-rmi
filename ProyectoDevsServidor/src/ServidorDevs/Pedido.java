package ServidorDevs;

import Interfaces.IPedido;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class Pedido extends UnicastRemoteObject implements IPedido {

    private int id_pedido;
    private int id_producto;
    private int id_venta;
    private int cantidad_producto;
    private double precio_unitario;
    private double precio_total;

    public Pedido() throws RemoteException {

    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_venta() {
        return id_venta;
    }

    public void setId_venta(int id_venta) {
        this.id_venta = id_venta;
    }

    public int getCantidad_producto() {
        return cantidad_producto;
    }

    public void setCantidad_producto(int cantidad_producto) {
        this.cantidad_producto = cantidad_producto;
    }

    public double getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(double precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public double getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(double precio_total) {
        this.precio_total = precio_total;
    }

    @Override
    public String getString() throws RemoteException {
        return String.format("Id Producto: %d\n, Id Pedido: %d\n, Id Venta: %d\n Cantidad_Producto: %d\n Precio Unitario %d\n Total Pedido: %d"
                , id_producto, id_pedido, id_venta, cantidad_producto, precio_unitario, precio_total);
    }

    public static IPedido fromMap(Map<String, Object> map) throws RemoteException {

        IPedido pedido = new Pedido();

        if (map.containsKey("id_pedido")) { 
            pedido.setId_pedido((int) map.get("id_pedido"));
        }

        if (map.containsKey("id_producto")) {
            pedido.setId_producto((int) map.get("id_producto"));
        }
        
        if (map.containsKey("id_venta")) {
            pedido.setId_venta((int) map.get("id_venta"));
        }
        if (map.containsKey("cantidad_producto")) {
            pedido.setCantidad_producto((int) map.get("cantidad_producto"));
        }
        if (map.containsKey("precio_unitario")) {
            pedido.setPrecio_unitario((double) map.get("precio_unitario"));
        }
        if (map.containsKey("precio_total")) {
            pedido.setPrecio_total((double) map.get("precio_total"));
        }

        
        return pedido;
    }

    public static Map<String, Object> toMap(IPedido pedido) throws RemoteException {
        Map<String, Object> datos = new HashMap<>();
        if (pedido.getId_pedido() != 0) {
            datos.put("id_pedido", pedido.getId_pedido());
        }
       
        if (pedido.getId_producto() != 0) {
            datos.put("id_producto", pedido.getId_producto());
        }
        if (pedido.getId_venta()!= 0) {
            datos.put("id_venta", pedido.getId_venta());
        }
        if (pedido.getCantidad_producto()!= 0) {
            datos.put("cantidad_producto", pedido.getCantidad_producto());
        }
        if (pedido.getPrecio_unitario()!= 0) {
            datos.put("precio_unitario", pedido.getPrecio_unitario());
        }
        if (pedido.getPrecio_total()!= 0) {
            datos.put("precio_total", pedido.getPrecio_total());
        }
        
        return datos;
    }

}
