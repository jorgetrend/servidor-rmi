package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jose Luis
 */
public interface IVentaController extends Remote, Estados {

    IVenta newInstance() throws RemoteException;

    int add(IVenta venta) throws RemoteException;

    int update(IVenta venta) throws RemoteException;

    int delete(IVenta venta) throws RemoteException;

    int delete(int idVenta) throws RemoteException;

    IVenta findOne(int idVenta) throws RemoteException;

    List<IVenta> list() throws RemoteException;

    List<IVenta> find(IVenta venta) throws RemoteException;

    List<String> getTime() throws RemoteException;

    List<String> getTime(String where) throws RemoteException;

    List<IVenta> listarFecha(String fechaInicio, String fechaFinal) throws RemoteException;
}
