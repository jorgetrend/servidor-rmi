package Interfaces;

public interface Estados {

    final int ADD_EXITO = 1;
    final int ADD_ID_DUPLICADO = 2;
    final int ADD_SIN_EXITO = 3;

    final int UPDATE_EXITO = 1;
    final int UPDATE_ID_INEXISTENTE = 2;
    final int UPDATE_ID_NULO = 3;
    final int UPDATE_SIN_EXITO = 4;

    final int DELETE_EXITO = 1;
    final int DELETE_ID_INEXISTENTE = 2;
    final int DELETE_ID_NULO = 3;
    final int DELETE_SIN_EXITO = 4;

    final String URL_ARCHIVO = ".\\Preferencias.dat";
    final int TAMANIO_PANEL = 30;
    final int TAMANIO_BOTON = 20;
}
