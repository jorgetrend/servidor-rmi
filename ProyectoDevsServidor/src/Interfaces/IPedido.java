package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IPedido extends Remote {

    public int getId_pedido() throws RemoteException;

    public void setId_pedido(int id_pedido) throws RemoteException;

    public int getId_producto() throws RemoteException;

    public void setId_producto(int id_producto) throws RemoteException;

    public int getId_venta() throws RemoteException;

    public void setId_venta(int id_producto) throws RemoteException;

    public int getCantidad_producto() throws RemoteException;

    public void setCantidad_producto(int cantidad_producto) throws RemoteException;

    public double getPrecio_unitario() throws RemoteException;

    public void setPrecio_unitario(double precio_unitario) throws RemoteException;

    public double getPrecio_total() throws RemoteException;

    public void setPrecio_total(double precio_total) throws RemoteException;

    String getString() throws RemoteException;
}
