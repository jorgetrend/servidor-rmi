package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

public interface IVenta extends Remote{

    public int getId_venta() throws RemoteException;

    void setId_venta(int id_venta) throws RemoteException;

    public String getEstado_venta() throws RemoteException;

    public void setEstado_venta(String estado_venta) throws RemoteException;

    public double getMonto_venta() throws RemoteException;

    public void setMonto_venta(double monto_venta) throws RemoteException;

    public Date getFecha_venta() throws RemoteException;

    public void setFecha_venta(Date fecha_venta) throws RemoteException;

    String getString() throws RemoteException;
}
