/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
/**
 *
 * @author jorge
 */
public interface IProducto extends Remote {

    public int getId_producto() throws RemoteException;
    
    public void setId_producto(int id_producto) throws RemoteException;

    public String getNombre_producto() throws RemoteException;

    public void setNombre_producto(String nombre_producto) throws RemoteException;

    public String getCodigo_producto() throws RemoteException;

    public void setCodigo_producto(String codigo_producto) throws RemoteException;

    public String getDescripcion_producto() throws RemoteException;

    public void setDescripcion_producto(String descripcion_producto) throws RemoteException;

    public double getPrecio_producto() throws RemoteException;

    public void setPrecio_producto(double precio_producto) throws RemoteException;
    
    String getString() throws RemoteException;
}
