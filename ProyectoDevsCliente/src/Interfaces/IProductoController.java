package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IProductoController extends Remote, Estados {
   
    IProducto newInstance() throws RemoteException;

    int add(IProducto producto) throws RemoteException;

    int update(IProducto producto) throws RemoteException;

    int delete(IProducto producto) throws RemoteException;
    
    int delete(int idProducto) throws RemoteException;

    IProducto findOne(int idProducto) throws RemoteException;
    
    List<IProducto> find( IProducto producto) throws RemoteException;
    
    List<IProducto> list() throws RemoteException;

   
}
