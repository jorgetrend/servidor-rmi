package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IPedidoController extends Remote, Estados {

    IPedido newInstance() throws RemoteException;

    int add(IPedido pedido) throws RemoteException;

    int update(IPedido pedido) throws RemoteException;

    int delete(IPedido pedido) throws RemoteException;

    int delete(int id_pedido) throws RemoteException;

    IPedido findOne(int id_pedido) throws RemoteException;
    
    int findMax() throws RemoteException;

    List<IPedido> find(IPedido pedido) throws RemoteException;

    List<IPedido> list() throws RemoteException;

}
