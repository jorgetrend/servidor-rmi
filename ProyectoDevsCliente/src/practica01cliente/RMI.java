package practica01cliente;

import Interfaces.IPedidoController;
import Interfaces.IProductoController;
import Interfaces.IVentaController;
import InterfazCliente.DialogAgregarProducto;
import InterfazCliente.PanelConfiguracion;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorgeMH
 */
public class RMI {

    private static IProductoController productoController;
    private static IVentaController ventaController;
    private static IPedidoController pedidoController;

    private static final String IP = PanelConfiguracion.getPropiedades().getProperty("IP");

    public static IProductoController getIProductoController() {
        System.err.println("Dirección "+IP);
        try {
            if (productoController == null) {
                productoController
                        = (IProductoController) Naming.lookup("rmi://" + IP + "/ProductoController");
            }
        } catch (NotBoundException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
            new DialogAgregarProducto(null, true, "Configuración").setVisible(true);

        }
        return productoController;
    }

    public static IVentaController getIVentaController() {
        try {
            if (ventaController == null) {
                ventaController
                        = (IVentaController) Naming.lookup("rmi://" + IP + "/VentaController");
            }
        } catch (NotBoundException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ventaController;
    }

    public static IPedidoController getIPedidoController() {
        try {
            if (pedidoController == null) {
                pedidoController
                        = (IPedidoController) Naming.lookup("rmi://" + IP + "/PedidoController");
            }
        } catch (NotBoundException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(RMI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pedidoController;
    }

}
