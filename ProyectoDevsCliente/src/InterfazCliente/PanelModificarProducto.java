package InterfazCliente;

import Interfaces.Estados;
import static Interfaces.Estados.TAMANIO_PANEL;
import Interfaces.IProducto;
import java.awt.Image;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import practica01cliente.RMI;

/**
 *
 * @author danni
 */
public class PanelModificarProducto extends javax.swing.JPanel implements Estados {

    private static JDialog dialogParent;
    private static Image logo;
    private final static String NOMBRE = "Ej. Producto de Prueba";
    private final static String CODIGO = "Ej. PS001";
    private final static String DESCRIPCION = "Ej. Descripción larga del producto.";
    private final IProducto productoRecibido;

    public PanelModificarProducto(JDialog dialogParent, IProducto producto) throws RemoteException {
        initComponents();
        this.dialogParent = dialogParent;
        productoRecibido = producto;
        nombreProductoTextField.setText(producto.getNombre_producto());
        codigoProductoTextField.setText(producto.getCodigo_producto());
        descripcionProductoTextField.setText(producto.getDescripcion_producto());
        precioProductoSpinner.setValue((Double) producto.getPrecio_producto());

        logo = new ImageIcon(("./src/media/pencil.png")).getImage();
        jLabTitulo.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_PANEL, TAMANIO_PANEL, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/check.png")).getImage();
        jButAceptar.setIcon(new ImageIcon(logo.getScaledInstance(15, 15, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/clear.png")).getImage();
        jButCancelar.setIcon(new ImageIcon(logo.getScaledInstance(15, 15, Image.SCALE_SMOOTH)));
    }

    PanelModificarProducto(Object object, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        nombreProductoLabel = new javax.swing.JLabel();
        codigoProductoLabel = new javax.swing.JLabel();
        descripcionProductoLabel = new javax.swing.JLabel();
        precioProductoLabel = new javax.swing.JLabel();
        nombreProductoTextField = new javax.swing.JTextField();
        codigoProductoTextField = new javax.swing.JTextField();
        descripcionProductoTextField = new javax.swing.JTextField();
        jButCancelar = new javax.swing.JButton();
        jButAceptar = new javax.swing.JButton();
        precioProductoSpinner = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        jLabel1.setText("jLabel1");

        jLabel4.setText("jLabel4");

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(500, 400));
        setPreferredSize(new java.awt.Dimension(500, 444));

        nombreProductoLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nombreProductoLabel.setText("Nombre:");

        codigoProductoLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        codigoProductoLabel.setText("Código:");

        descripcionProductoLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        descripcionProductoLabel.setText("Descripción:");

        precioProductoLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        precioProductoLabel.setText("Precio:");

        nombreProductoTextField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        nombreProductoTextField.setText("Ej. Producto de Prueba");
        nombreProductoTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nombreProductoTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                nombreProductoTextFieldFocusLost(evt);
            }
        });
        nombreProductoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreProductoTextFieldActionPerformed(evt);
            }
        });

        codigoProductoTextField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        codigoProductoTextField.setText("Ej. PS001");
        codigoProductoTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                codigoProductoTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                codigoProductoTextFieldFocusLost(evt);
            }
        });
        codigoProductoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codigoProductoTextFieldActionPerformed(evt);
            }
        });

        descripcionProductoTextField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        descripcionProductoTextField.setText("Ej. Descripción larga del producto.");
        descripcionProductoTextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                descripcionProductoTextFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                descripcionProductoTextFieldFocusLost(evt);
            }
        });
        descripcionProductoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descripcionProductoTextFieldActionPerformed(evt);
            }
        });

        jButCancelar.setBackground(new java.awt.Color(204, 0, 0));
        jButCancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButCancelar.setForeground(new java.awt.Color(204, 0, 0));
        jButCancelar.setText("Cancelar");
        jButCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButCancelarActionPerformed(evt);
            }
        });

        jButAceptar.setBackground(new java.awt.Color(0, 102, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(0, 102, 255));
        jButAceptar.setText("Aceptar");
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        precioProductoSpinner.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        precioProductoSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, 10000.0d, 0.5d));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabTitulo.setText("Editar Producto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(codigoProductoLabel)
                    .addComponent(descripcionProductoLabel)
                    .addComponent(nombreProductoLabel)
                    .addComponent(precioProductoLabel)
                    .addComponent(descripcionProductoTextField)
                    .addComponent(codigoProductoTextField)
                    .addComponent(nombreProductoTextField)
                    .addComponent(precioProductoSpinner)
                    .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
                    .addComponent(jSeparator1))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nombreProductoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nombreProductoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(codigoProductoLabel)
                .addGap(9, 9, 9)
                .addComponent(codigoProductoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(descripcionProductoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(descripcionProductoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(precioProductoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(precioProductoSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        try {

            String nombreProducto = nombreProductoTextField.getText();
            String codigoProducto = codigoProductoTextField.getText();
            String descripcionProducto = descripcionProductoTextField.getText();
            double precioProducto = Double.valueOf(precioProductoSpinner.getValue().toString());

            IProducto producto = RMI.getIProductoController().newInstance();

            /*Validación para nombre del producto*/
            if (nombreProducto.length() == 0 || nombreProducto.equals(NOMBRE)) {
                JOptionPane.showMessageDialog(
                        this,
                        "Ingrese nombre del producto:",
                        "Validación",
                        JOptionPane.ERROR_MESSAGE
                );
                nombreProductoTextField.requestFocus();
                return;
            } else {
                producto.setNombre_producto(nombreProducto);
            } // end if-else

            /*Validación para el código del producto*/
            if (codigoProducto.length() == 0 || codigoProducto.equals(CODIGO)) {
                JOptionPane.showMessageDialog(
                        this,
                        "Ingrese el código del  producto: ",
                        "Validación",
                        JOptionPane.ERROR_MESSAGE
                );
                codigoProductoTextField.requestFocus();
                return;
            } else {
                producto.setCodigo_producto(codigoProducto);
            } // end if-else

            /*Validación para la descripción del producto*/
            if (descripcionProducto.length() == 0 || descripcionProducto.equals(DESCRIPCION)) {
                JOptionPane.showMessageDialog(
                        this,
                        "Ingrese descripción del  producto: ",
                        "Validación",
                        JOptionPane.ERROR_MESSAGE
                );
                descripcionProductoTextField.requestFocus();
                return;
            } else {
                producto.setDescripcion_producto(descripcionProducto);
            } // end if-else

            /*Validación para precio del producto*/
            if (precioProducto == 0) {
                JOptionPane.showMessageDialog(
                        this,
                        "Ingrese precio del  producto: ",
                        "Validación",
                        JOptionPane.ERROR_MESSAGE
                );
                precioProductoSpinner.requestFocus();
                return;
            } else {
                producto.setPrecio_producto(precioProducto);
            } // end if-else

            //Insertar datos
            producto.setId_producto(productoRecibido.getId_producto());
            int respuesta = RMI.getIProductoController().update(producto);
            if (respuesta == Estados.UPDATE_EXITO) {
                JOptionPane.showMessageDialog(
                        this,
                        "Producto correctamente modificado",
                        "Finalizado",
                        JOptionPane.INFORMATION_MESSAGE
                );
                PanelProducto.getPanelProducto().refrescarTabla();
            } else if (respuesta == Estados.UPDATE_SIN_EXITO) {
                System.out.println(respuesta);
                JOptionPane.showMessageDialog(
                        this,
                        "No se pudo realizar el registro del producto",
                        "Acción no completada",
                        JOptionPane.ERROR_MESSAGE);
                PanelProducto.getPanelProducto().refrescarTabla();
            }
        } catch (RemoteException ex) {
            Logger.getLogger(PanelModificarProducto.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            dialogParent.dispose();
        }

    }//GEN-LAST:event_jButAceptarActionPerformed

    private void jButCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButCancelarActionPerformed
        dialogParent.dispose();
    }//GEN-LAST:event_jButCancelarActionPerformed

    private void nombreProductoTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nombreProductoTextFieldFocusGained
        if (nombreProductoTextField.getText().equals(NOMBRE)) {
            nombreProductoTextField.setText("");
        }
    }//GEN-LAST:event_nombreProductoTextFieldFocusGained

    private void nombreProductoTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nombreProductoTextFieldFocusLost
        if (nombreProductoTextField.getText().equals("")) {
            nombreProductoTextField.setText(NOMBRE);
        }
    }//GEN-LAST:event_nombreProductoTextFieldFocusLost

    private void nombreProductoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreProductoTextFieldActionPerformed
        nombreProductoTextField.setText("");
    }//GEN-LAST:event_nombreProductoTextFieldActionPerformed

    private void codigoProductoTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codigoProductoTextFieldFocusGained
        if (codigoProductoTextField.getText().equals(CODIGO)) {
            codigoProductoTextField.setText("");
        }
    }//GEN-LAST:event_codigoProductoTextFieldFocusGained

    private void codigoProductoTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codigoProductoTextFieldFocusLost
        if (codigoProductoTextField.getText().equals("")) {
            codigoProductoTextField.setText(CODIGO);
        }
    }//GEN-LAST:event_codigoProductoTextFieldFocusLost

    private void codigoProductoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codigoProductoTextFieldActionPerformed
        codigoProductoTextField.setText("");
    }//GEN-LAST:event_codigoProductoTextFieldActionPerformed

    private void descripcionProductoTextFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_descripcionProductoTextFieldFocusGained
        if (descripcionProductoTextField.getText().equals(DESCRIPCION)) {
            descripcionProductoTextField.setText("");
        }
    }//GEN-LAST:event_descripcionProductoTextFieldFocusGained

    private void descripcionProductoTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_descripcionProductoTextFieldFocusLost
        if (descripcionProductoTextField.getText().equals("")) {
            descripcionProductoTextField.setText(DESCRIPCION);
        }
    }//GEN-LAST:event_descripcionProductoTextFieldFocusLost

    private void descripcionProductoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descripcionProductoTextFieldActionPerformed
        descripcionProductoTextField.setText("");
    }//GEN-LAST:event_descripcionProductoTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel codigoProductoLabel;
    private static javax.swing.JTextField codigoProductoTextField;
    private javax.swing.JLabel descripcionProductoLabel;
    private static javax.swing.JTextField descripcionProductoTextField;
    private static javax.swing.JButton jButAceptar;
    private static javax.swing.JButton jButCancelar;
    private static javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel nombreProductoLabel;
    private static javax.swing.JTextField nombreProductoTextField;
    private javax.swing.JLabel precioProductoLabel;
    private static javax.swing.JSpinner precioProductoSpinner;
    // End of variables declaration//GEN-END:variables

}
