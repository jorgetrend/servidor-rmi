package InterfazCliente;

import Interfaces.IProducto;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import practica01cliente.RMI;

/**
 * @author Carlos
 */
public class PanelBusqueda extends javax.swing.JPanel {

    private static PanelBusqueda panel;
    private static DialogBusqueda padre;
    private Vector<Vector> datos = new Vector<>();

    private PanelBusqueda() {
        initComponents();
        rellenarJList();
        jList2.setSelectedIndex(0);
    }

    public static PanelBusqueda getPanelBusqueda(DialogBusqueda parent) {
        if (panel == null) {
            panel = new PanelBusqueda();
        }
        padre = parent;
        return panel;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel5 = new javax.swing.JLabel();
        jlbCodigo = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jlbNombre = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jlbPrecio = new javax.swing.JLabel();
        jlbCantidad = new javax.swing.JLabel();
        jSpinCantidad = new javax.swing.JSpinner();
        jLabTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jButAceptar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Productos Registrados");

        jScrollPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jScrollPane1.setAlignmentX(1.0F);
        jScrollPane1.setAlignmentY(1.0F);

        jList2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));
        jList2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jList2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jList2KeyPressed(evt);
            }
        });
        jList2.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList2ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList2);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel5.setText("Código:");

        jlbCodigo.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jlbCodigo.setText("Código del producto");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel6.setText("Nombre:");

        jlbNombre.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jlbNombre.setText("Nombre del producto");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel7.setText("Precio");

        jlbPrecio.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jlbPrecio.setText("Precio del producto");

        jlbCantidad.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jlbCantidad.setText("Cantidad");

        jSpinCantidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jSpinCantidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabTitulo.setText("Búsqueda de productos");

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jButAceptar.setBackground(new java.awt.Color(0, 153, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(255, 255, 255));
        jButAceptar.setText("Agregar Producto");
        jButAceptar.setToolTipText("");
        jButAceptar.setAutoscrolls(true);
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jlbNombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jlbPrecio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlbCantidad, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSpinCantidad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                                    .addComponent(jlbCodigo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(28, 28, 28))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlbCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlbPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(jlbCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSpinCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jList2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jList2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            padre.dispose();
        }//End if-else.
    }//GEN-LAST:event_jList2KeyPressed

    private void jList2ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList2ValueChanged

        if (jList2.getSelectedValue().equalsIgnoreCase("") || jList2.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "Selecciona un elemento de la lista\n     para continuar",
                    "¡Ningún elemento seleccionado!", JOptionPane.WARNING_MESSAGE);
        } else {
            jlbCodigo.setText("" + datos.get(jList2.getSelectedIndex()).elementAt(2));
            jlbNombre.setText("" + datos.get(jList2.getSelectedIndex()).elementAt(1));
            jlbPrecio.setText("" + datos.get(jList2.getSelectedIndex()).elementAt(4));
        }//End if-else.
    }//GEN-LAST:event_jList2ValueChanged

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        if (jList2.getSelectedValue().equalsIgnoreCase("") || jList2.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "Selecciona un elemento de la lista\n     para continuar",
                    "¡Ningún elemento seleccionado!", JOptionPane.WARNING_MESSAGE);
        } else {
            agregarProducto();
            padre.dispose();
        }//End if-else.
    }//GEN-LAST:event_jButAceptarActionPerformed

    private void rellenarJList() {
        DefaultListModel modelo = new DefaultListModel();

        try {
            List<IProducto> listProductos;
            listProductos = RMI.getIProductoController().list();

            for (IProducto producto : listProductos) {
                Vector registro = new Vector();

                registro.add(producto.getId_producto());
                registro.add(producto.getNombre_producto());
                registro.add(producto.getCodigo_producto());
                registro.add(producto.getDescripcion_producto());
                registro.add(producto.getPrecio_producto());
                datos.add(registro);
                modelo.addElement(producto.getNombre_producto());
            }

            jList2.setModel(modelo);
            jlbCodigo.setText("" + datos.get(0).elementAt(2));
            jlbNombre.setText("" + datos.get(0).elementAt(1));
            jlbPrecio.setText("" + datos.get(0).elementAt(4));

        } catch (RemoteException ex) {
            Logger.getLogger(PanelProducto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void agregarProducto() {
        JTable jTabProducto = PanelNuevaVenta.getPanelVentas().getjTabProducto();
        Vector venta = new Vector();
        int seleccion = jList2.getSelectedIndex();
        int cantidad = Integer.valueOf(jSpinCantidad.getValue().toString());
        double precio = Double.parseDouble("" + datos.get(seleccion).elementAt(4));
        venta.add(datos.get(seleccion).elementAt(0));
        venta.add("" + datos.get(seleccion).elementAt(1));
        venta.add(cantidad);
        venta.add(precio);
        venta.add((cantidad * precio));
        try {
            DefaultTableModel tableModel = (DefaultTableModel) jTabProducto.getModel();
            tableModel.addRow(venta);
        } catch (NullPointerException ex) {
            //Guardar errores.
        } finally {
            jList2.setSelectedIndex(0);
            jSpinCantidad.setValue(1);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JButton jButAceptar;
    private static javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JList<String> jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner jSpinCantidad;
    private javax.swing.JLabel jlbCantidad;
    private javax.swing.JLabel jlbCodigo;
    private javax.swing.JLabel jlbNombre;
    private javax.swing.JLabel jlbPrecio;
    // End of variables declaration//GEN-END:variables

}
