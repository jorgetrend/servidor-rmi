package InterfazCliente;

import static Interfaces.Estados.URL_ARCHIVO;
import java.awt.Image;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * @author Carlos Daniel
 */
public class SplashScreen extends javax.swing.JFrame implements Runnable {

    //-----------------VARIABLES----------------------
    private static SplashScreen splashScreen;
    private Thread tiempo = null;
    private static final int tamanio = 123;
    private Image icono = new ImageIcon(("./src/media/shopping-cart.png")).getImage();
    private boolean comprobacion;
    //------------------------------------------------

    private SplashScreen() {
        setUndecorated(true);
        initComponents();
        jlbLogo.setIcon(new ImageIcon(icono.getScaledInstance(tamanio, tamanio, Image.SCALE_SMOOTH)));
        setTitle("Devs Market");
        setIconImage(new ImageIcon(("./src/media/icono.png")).getImage());
        setLocationRelativeTo(null);
        comprobacion = comprobarArchivo();
        //Crea un hilo para la duración en pantalla del Splash.
        tiempo = new Thread(this);
        tiempo.start();
    }

    public static SplashScreen getSplashScreen() {
        if (splashScreen == null) {
            splashScreen = new SplashScreen();
        }
        return splashScreen;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlbTitulo = new javax.swing.JLabel();
        rSProgressBarA = new rojerusan.RSProgressBarAnimated();
        jlbTitulo1 = new javax.swing.JLabel();
        jlbTitulo2 = new javax.swing.JLabel();
        jlbLogo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(0, 0));
        setMinimumSize(new java.awt.Dimension(400, 300));
        setUndecorated(true);
        setResizable(false);
        setSize(new java.awt.Dimension(400, 300));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        jPanel1.setPreferredSize(new java.awt.Dimension(720, 480));

        jlbTitulo.setBackground(new java.awt.Color(255, 255, 255));
        jlbTitulo.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        jlbTitulo.setOpaque(true);

        rSProgressBarA.setForeground(new java.awt.Color(51, 153, 255));
        rSProgressBarA.setValue(0);
        rSProgressBarA.setAnimated(false);
        rSProgressBarA.setColorSelForeground(new java.awt.Color(0, 0, 0));
        rSProgressBarA.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        rSProgressBarA.setString("Comprobando el Sistema...");
        rSProgressBarA.setVelocidad(40);

        jlbTitulo1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jlbTitulo1.setForeground(new java.awt.Color(3, 3, 3));
        jlbTitulo1.setText("Devs");

        jlbTitulo2.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jlbTitulo2.setForeground(new java.awt.Color(102, 102, 102));
        jlbTitulo2.setText("Market");

        jlbLogo.setBackground(new java.awt.Color(255, 255, 255));
        jlbLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlbTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jlbLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(82, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jlbTitulo1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jlbTitulo2)
                        .addGap(222, 222, 222))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(rSProgressBarA, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(81, 81, 81))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jlbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jlbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbTitulo2)
                    .addComponent(jlbTitulo1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rSProgressBarA, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 665, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SplashScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SplashScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SplashScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SplashScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SplashScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlbLogo;
    private javax.swing.JLabel jlbTitulo;
    private javax.swing.JLabel jlbTitulo1;
    private javax.swing.JLabel jlbTitulo2;
    private rojerusan.RSProgressBarAnimated rSProgressBarA;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {
        //Crea la función para que durante 3 segundos se muestre el SplashScreen.
        while (tiempo != null) {
            try {
                Thread.sleep(300);
                rSProgressBarA.setValue(10);
                Thread.sleep(300);
                rSProgressBarA.setValue(20);
                Thread.sleep(300);
                rSProgressBarA.setValue(30);
                Thread.sleep(500);
                rSProgressBarA.setValue(40);
                rSProgressBarA.setString("Comprobando Datos del Sistema...");
                Thread.sleep(300);
                rSProgressBarA.setValue(40);
                Thread.sleep(300);
                rSProgressBarA.setValue(50);
                Thread.sleep(300);
                if (comprobacion) {
                    rSProgressBarA.setValue(60);
                    rSProgressBarA.setString("Cargando Interfaz...");
                    Thread.sleep(500);
                    rSProgressBarA.setValue(70);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(80);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(90);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(100);
                    Thread.sleep(300);
                    rSProgressBarA.setString("¡Bienvenido de vuelta!");
                    Thread.sleep(700);
                    tiempo = null;
                    new FramePrincipal().setVisible(true);
                } else {
                    rSProgressBarA.setValue(60);
                    rSProgressBarA.setString("¡Bienvenido, Por favor espere un momento!...");
                    Thread.sleep(500);
                    rSProgressBarA.setValue(70);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(80);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(90);
                    Thread.sleep(300);
                    rSProgressBarA.setValue(100);
                    Thread.sleep(300);
                    rSProgressBarA.setString("Cargando Ventana de Configuración...");
                    Thread.sleep(700);
                    tiempo = null;
                    new DialogAgregarProducto(this, true, "Configuración").setVisible(true);
                }
                this.dispose();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ocurrió un error fatal. \n Error de conexión .",
                        "Error en el sistema :(", JOptionPane.ERROR_MESSAGE);
                System.err.println(e);
                System.err.println(e.getLocalizedMessage());
                System.exit(1);
            } finally {
                if (this != null) {
                    this.dispose();
                }
            } //End try-catch.
        }//End while.        
    }//End Run.

    private static File archivo = null;

    private boolean comprobarArchivo() {
        archivo = new File(URL_ARCHIVO);
        return archivo.exists();
    }
}
