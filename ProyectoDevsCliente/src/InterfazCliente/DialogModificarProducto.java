package InterfazCliente;

import Interfaces.IProducto;
import java.rmi.RemoteException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author danni
 */
public class DialogModificarProducto extends javax.swing.JDialog {

    public DialogModificarProducto(java.awt.Frame parent, boolean modal, IProducto producto) throws RemoteException {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Devs Market");
        setIconImage(new ImageIcon(("./src/media/icono.png")).getImage());

        try {
            //Establece el estilo de la ventana como el predeterminado del sistema. 
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            System.exit(1);
        }//End try-catch.
        jPanContenedor.add(new PanelModificarProducto(this, producto));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanContenedor = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(490, 475));
        setPreferredSize(new java.awt.Dimension(490, 475));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanContenedor.setBackground(new java.awt.Color(255, 255, 255));
        jPanContenedor.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        int seleccion = JOptionPane.showConfirmDialog(this, "   ¿Realmente deseas cancelar \n     la operación actual?",
                "Confirmación de seguridad", JOptionPane.YES_NO_OPTION);
        if (seleccion == JOptionPane.YES_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanContenedor;
    // End of variables declaration//GEN-END:variables
}
