package InterfazCliente;

import Interfaces.IVenta;
import java.awt.Image;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import practica01cliente.RMI;

/**
 * @author Carlos
 */
public class PanelFechas extends javax.swing.JPanel {

    private static PanelFechas panel;
    private static DialogFechas padre;

    private Image logo;

    private PanelFechas() {
        initComponents();
        logo = new ImageIcon(("./src/media/history.png")).getImage();
        jLabTitulo.setIcon(new ImageIcon(logo.getScaledInstance(20, 20, Image.SCALE_SMOOTH)));
    }

    public static PanelFechas getPanelFechas(DialogFechas parent) {
        if (panel == null) {
            panel = new PanelFechas();
        }
        padre = parent;
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabTitulo2 = new javax.swing.JLabel();
        jLabTitulo3 = new javax.swing.JLabel();
        jButAceptar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo = new javax.swing.JLabel();
        jDateInicio = new com.toedter.calendar.JDateChooser();
        jDateTermino = new com.toedter.calendar.JDateChooser();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabTitulo2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabTitulo2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo2.setText("Fecha de Término:");
        jLabTitulo2.setIconTextGap(8);

        jLabTitulo3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabTitulo3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo3.setText("Fecha de inicio:");
        jLabTitulo3.setIconTextGap(8);

        jButAceptar.setBackground(new java.awt.Color(0, 153, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(255, 255, 255));
        jButAceptar.setText("Aceptar");
        jButAceptar.setToolTipText("Editar Persona");
        jButAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButAceptar.setIconTextGap(5);
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Reporte por fechas");
        jLabTitulo.setIconTextGap(8);

        jDateInicio.setDateFormatString("yyyy-MM-dd");

        jDateTermino.setDateFormatString("yyyy-MM-dd");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabTitulo2)
                .addContainerGap(314, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                            .addComponent(jSeparator1)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(jLabTitulo3, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(252, 252, 252))
                            .addComponent(jDateInicio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jDateTermino, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabTitulo3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jLabTitulo2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jDateTermino, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 95, Short.MAX_VALUE)
                .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        try {
            if (!jDateInicio.toString().equals("") && !jDateTermino.getDate().toString().equals("")) {

                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                String inicio = formato.format(jDateInicio.getDate());
                String termino = formato.format(jDateTermino.getDate());
                SimpleDateFormat formatoLabel = new SimpleDateFormat("dd/MM/yyyy");
                String inicioLabel = formatoLabel.format(jDateInicio.getDate());
                String terminoLabel = formatoLabel.format(jDateTermino.getDate());
                JLabel label = PanelVentaRealizada.getjLabSubtitulo();
                label.setText("Ventas realizadas del " + inicioLabel + " al " + terminoLabel);
                label.setVisible(true);
                try {
                    Vector<Vector> datos = new Vector<>();
                    List<IVenta> listVenta;
                    listVenta = RMI.getIVentaController().listarFecha(inicio, termino);
                    String where = "fecha_venta BETWEEN '" + inicio + " 00:00:00' AND '"
                            + termino + " 23:59:59';";
                    List<String> fechas = RMI.getIVentaController().getTime(where);
                    int contador = 0;
                    for (IVenta venta : listVenta) {
                        Vector registro = new Vector();
                        registro.add(venta.getId_venta());
                        registro.add(venta.getEstado_venta());
                        registro.add(venta.getMonto_venta());
                        registro.add(fechas.get(contador));
                        datos.add(registro);
                        contador++;
                    }

                    Vector<String> columnas = new Vector<>();
                    columnas.add("Número de Venta");
                    columnas.add("Estado de la venta");
                    columnas.add("Monto Total");
                    columnas.add("Fecha");

                    PanelVentaRealizada.getjTabVentaRealizada().setModel(new javax.swing.table.DefaultTableModel(datos, columnas));
                } catch (RemoteException ex) {
                    Logger.getLogger(PanelProducto.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    jDateInicio.setDate(null);
                    jDateTermino.setDate(null);
                    padre.dispose();
                }
            }
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Algún campo está vacío",
                     "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButAceptarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButAceptar;
    private com.toedter.calendar.JDateChooser jDateInicio;
    private com.toedter.calendar.JDateChooser jDateTermino;
    private javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabTitulo2;
    private javax.swing.JLabel jLabTitulo3;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables

}
