package InterfazCliente;

import Interfaces.Estados;
import java.awt.Color;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

/**
 * @author Carlos
 */
public class PanelMenu extends javax.swing.JPanel implements Estados {

    private Image logo;
    private static PanelMenu panel;
    private JPanel panelPrincipal;

    private PanelMenu() {
        initComponents();
        logo = new ImageIcon(("./src/media/round-user.png")).getImage();
        jLabIcono.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_MENU, TAMANIO_MENU, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/money.png")).getImage();
        jButVenta.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_BOTON, TAMANIO_BOTON, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/basket.png")).getImage();
        jButProductos.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_BOTON, TAMANIO_BOTON, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/history.png")).getImage();
        jButVentas.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_BOTON, TAMANIO_BOTON, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/logout.png")).getImage();
        jButSalir.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_BOTON, TAMANIO_BOTON, Image.SCALE_SMOOTH)));
        panelPrincipal = FramePrincipal.getPanelPrincipal();
        activarPanel(jButVenta, jButProductos, jButVentas, 1);
        jLabNombre.setText("" + PanelConfiguracion.getPropiedades().getProperty("Nombre"));

    }

    public static PanelMenu getPanelMenu() {
        if (panel == null) {
            panel = new PanelMenu();
        }
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabNombre = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabIcono = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jButSalir = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jButVenta = new javax.swing.JToggleButton();
        jButProductos = new javax.swing.JToggleButton();
        jButVentas = new javax.swing.JToggleButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 1, new java.awt.Color(0, 0, 0)));

        jLabNombre.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabNombre.setText("Carlos Daniel");

        jLabel2.setText("Administrador");

        jLabIcono.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jSeparator1.setBackground(new java.awt.Color(0, 153, 255));
        jSeparator1.setForeground(new java.awt.Color(0, 153, 255));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("©2020 Devs Inc.");

        jButSalir.setBackground(new java.awt.Color(204, 0, 0));
        jButSalir.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButSalir.setForeground(new java.awt.Color(255, 255, 255));
        jButSalir.setText("Salir");
        jButSalir.setToolTipText("");
        jButSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButSalirActionPerformed(evt);
            }
        });

        jSeparator2.setBackground(new java.awt.Color(0, 153, 255));
        jSeparator2.setForeground(new java.awt.Color(0, 153, 255));

        jButVenta.setBackground(new java.awt.Color(0, 153, 255));
        jButVenta.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButVenta.setForeground(new java.awt.Color(255, 255, 255));
        jButVenta.setText("Nueva Venta");
        jButVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButVentaActionPerformed(evt);
            }
        });

        jButProductos.setBackground(new java.awt.Color(0, 153, 255));
        jButProductos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButProductos.setForeground(new java.awt.Color(255, 255, 255));
        jButProductos.setText("Productos");
        jButProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButProductosActionPerformed(evt);
            }
        });

        jButVentas.setBackground(new java.awt.Color(0, 153, 255));
        jButVentas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButVentas.setForeground(new java.awt.Color(255, 255, 255));
        jButVentas.setText("Ventas");
        jButVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButVentasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabIcono, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(13, 13, 13)
                                        .addComponent(jLabNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(5, 5, 5)
                                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jSeparator2)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButProductos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabIcono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                .addComponent(jButVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(jButSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButSalirActionPerformed
        int seleccion = JOptionPane.showConfirmDialog(null, "     ¿Realmente deseas salir?",
                "Confirmación de seguridad", JOptionPane.YES_NO_OPTION);
        if (seleccion == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_jButSalirActionPerformed

    private void jButVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButVentaActionPerformed
        activarPanel(jButVenta, jButProductos, jButVentas, 1);
        cambiaPanel(panelPrincipal, PanelNuevaVenta.getPanelVentas());
    }//GEN-LAST:event_jButVentaActionPerformed

    private void jButProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButProductosActionPerformed
        activarPanel(jButVenta, jButProductos, jButVentas, 2);
        cambiaPanel(panelPrincipal, PanelProducto.getPanelProducto());
    }//GEN-LAST:event_jButProductosActionPerformed

    private void jButVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButVentasActionPerformed
        activarPanel(jButVenta, jButProductos, jButVentas, 3);
        cambiaPanel(panelPrincipal, PanelVentaRealizada.getPanelVentaR());
    }//GEN-LAST:event_jButVentasActionPerformed

    private void cambiaPanel(JPanel container, JPanel content) {
        JPanel contenedor;
        JPanel contenido;
        contenedor = container;
        contenido = content;
        contenedor.removeAll();
        contenedor.revalidate();
        contenedor.repaint();

        contenedor.add(contenido);
        contenedor.revalidate();
        contenedor.repaint();
    }

    private final Color colorNormal = new Color(0, 153, 255);
    private final Color colorResaltado = new Color(0, 79, 249);

    private void activarPanel(JToggleButton jButVenta, JToggleButton jButProductos,
            JToggleButton jButVentas, int Posicion) {

        switch (Posicion) {
            case 1:
                jButVenta.setSelected(true);
                jButVenta.setBackground(colorResaltado);
                jButProductos.setSelected(false);
                jButProductos.setBackground(colorNormal);
                jButVentas.setSelected(false);
                jButVentas.setBackground(colorNormal);
                break;
            case 2:
                jButVenta.setSelected(false);
                jButVenta.setBackground(colorNormal);
                jButProductos.setSelected(true);
                jButProductos.setBackground(colorResaltado);
                jButVentas.setSelected(false);
                jButVentas.setBackground(colorNormal);
                break;
            case 3:
                jButVenta.setSelected(false);
                jButVenta.setBackground(colorNormal);
                jButProductos.setSelected(false);
                jButProductos.setBackground(colorNormal);
                jButVentas.setSelected(true);
                jButVentas.setBackground(colorResaltado);
                break;

        }//End
    }//End function activarPanel.

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton jButProductos;
    private javax.swing.JButton jButSalir;
    private javax.swing.JToggleButton jButVenta;
    private javax.swing.JToggleButton jButVentas;
    private javax.swing.JLabel jLabIcono;
    private javax.swing.JLabel jLabNombre;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables

}
