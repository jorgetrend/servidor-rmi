package InterfazCliente;

import Interfaces.IPedido;
import Interfaces.IProducto;
import Interfaces.IVenta;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import practica01cliente.RMI;

/**
 * @author Carlos
 */
public class PanelVerVenta extends javax.swing.JPanel {

    private static PanelVerVenta panel;
    private static DialogVerVenta padre;

    private PanelVerVenta() {
        initComponents();
    }

    public static PanelVerVenta getPanelVerVentas(DialogVerVenta dialog, int idVenta) {
        if (panel == null) {
            panel = new PanelVerVenta();
        }
        padre = dialog;
        rellenarTabla(idVenta);
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTabProducto = new rojerusan.RSTableMetro();
        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo = new javax.swing.JLabel();
        jLabMensaje = new javax.swing.JLabel();
        jButAceptar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabMonto = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jTabProducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTabProducto.setAltoHead(40);
        jTabProducto.setColorBackgoundHead(new java.awt.Color(0, 0, 0));
        jTabProducto.setColorFilasForeground1(new java.awt.Color(0, 0, 0));
        jTabProducto.setColorFilasForeground2(new java.awt.Color(0, 0, 0));
        jTabProducto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabProducto.setFuenteFilas(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabProducto.setFuenteFilasSelect(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTabProducto.setGrosorBordeFilas(0);
        jTabProducto.setMultipleSeleccion(false);
        jTabProducto.setRowHeight(32);
        jTabProducto.getTableHeader().setResizingAllowed(false);
        jTabProducto.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTabProducto);

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Detalles de la Venta");
        jLabTitulo.setIconTextGap(8);

        jLabMensaje.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabMensaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabMensaje.setText("Visualiza los productos que componen la venta seleccionada.");
        jLabMensaje.setIconTextGap(8);

        jButAceptar.setBackground(new java.awt.Color(0, 153, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(255, 255, 255));
        jButAceptar.setText("Aceptar");
        jButAceptar.setToolTipText("");
        jButAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButAceptar.setIconTextGap(5);
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Monto Total");

        jLabMonto.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabMonto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabMonto.setText("000.00");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                                .addGap(356, 356, 356))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabMensaje, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(56, 56, 56))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabMensaje)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(28, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabMonto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(16, 16, 16))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        padre.dispose();
    }//GEN-LAST:event_jButAceptarActionPerformed

    private static void rellenarTabla(int idVenta) {
        Vector<Vector> datos = new Vector<>();
        try {
            IPedido pedido = RMI.getIPedidoController().newInstance();
            pedido.setId_venta(idVenta);
            List<IPedido> pedidos = RMI.getIPedidoController().find(pedido);
            List<IProducto> productos = RMI.getIProductoController().list();
            for (IPedido pedidoTemp : pedidos) {
                Vector registro = new Vector();

                for (IProducto productoTemp : productos) {
                    if (productoTemp.getId_producto() == pedidoTemp.getId_producto()) {
                        registro.add(productoTemp.getNombre_producto());
                        break;
                    }
                }
                registro.add(pedidoTemp.getCantidad_producto());
                registro.add(pedidoTemp.getPrecio_unitario());
                registro.add(pedidoTemp.getPrecio_total());

                datos.add(registro);
            }

            Vector<String> columnas = new Vector<>();
            columnas.add("Nombre del Producto");
            columnas.add("Cantidad del producto");
            columnas.add("Precio Unitario");
            columnas.add("Precio Total");

            jTabProducto.setModel(new javax.swing.table.DefaultTableModel(datos, columnas));

            IVenta venta = RMI.getIVentaController().findOne(idVenta);
            jLabMonto.setText("$ " + venta.getMonto_venta());
            
            
        } catch (RemoteException ex) {
            Logger.getLogger(PanelVerVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButAceptar;
    private javax.swing.JLabel jLabMensaje;
    private static javax.swing.JLabel jLabMonto;
    private javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private static rojerusan.RSTableMetro jTabProducto;
    // End of variables declaration//GEN-END:variables

}
