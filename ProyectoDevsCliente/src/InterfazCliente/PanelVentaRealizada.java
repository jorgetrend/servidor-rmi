package InterfazCliente;

import static Interfaces.Estados.TAMANIO_BOTON;
import static Interfaces.Estados.TAMANIO_PANEL;
import Interfaces.IVenta;
import Interfaces.IVentaController;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import practica01cliente.RMI;
import rojerusan.RSTableMetro;

/**
 *
 * @author jorge
 */
public class PanelVentaRealizada extends javax.swing.JPanel {

    private static PanelVentaRealizada panelVentaR;
    private static final String TEXTO_GUIA = "Ingresa el número de la venta a buscar.";
    private Image logo;

    private PanelVentaRealizada() {
        initComponents();
        refrescarTabla();
        jTexTexto.setText(TEXTO_GUIA);
        jButFechas.setBackground(new Color(0, 153, 255));
        logo = new ImageIcon(("./src/media/magnifying.png")).getImage();
        jButBuscar.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_BOTON, TAMANIO_BOTON, Image.SCALE_SMOOTH)));
        logo = new ImageIcon(("./src/media/history.png")).getImage();
        jLabTitulo.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_PANEL, TAMANIO_PANEL, Image.SCALE_SMOOTH)));
    }

    public static PanelVentaRealizada getPanelVentaR() {
        if (panelVentaR == null) {
            panelVentaR = new PanelVentaRealizada();
        }
        refrescarTabla();
        jTexTexto.setText(TEXTO_GUIA);
        jLabSubtitulo.setVisible(false);
        return panelVentaR;
    }

    public static RSTableMetro getjTabVentaRealizada() {
        return jTabVentaRealizada;
    }

    public static JLabel getjLabSubtitulo() {
        return jLabSubtitulo;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabMensaje = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTabVentaRealizada = new rojerusan.RSTableMetro();
        jButVerVenta = new javax.swing.JButton();
        jButCancelar = new javax.swing.JButton();
        jTexTexto = new javax.swing.JTextField();
        jButBuscar = new javax.swing.JButton();
        jLabSubtitulo = new javax.swing.JLabel();
        jButFechas = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jButActual = new javax.swing.JButton();
        jButReporte = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(800, 650));
        setPreferredSize(new java.awt.Dimension(800, 650));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Ventas Realizadas");
        jLabTitulo.setIconTextGap(8);

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabMensaje.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabMensaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabMensaje.setText("Visualiza todas las ventas realizadas en el sistema.");
        jLabMensaje.setIconTextGap(8);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jTabVentaRealizada.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTabVentaRealizada.setAltoHead(40);
        jTabVentaRealizada.setColorBackgoundHead(new java.awt.Color(0, 0, 0));
        jTabVentaRealizada.setColorFilasForeground1(new java.awt.Color(0, 0, 0));
        jTabVentaRealizada.setColorFilasForeground2(new java.awt.Color(0, 0, 0));
        jTabVentaRealizada.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabVentaRealizada.setFuenteFilas(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabVentaRealizada.setFuenteFilasSelect(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTabVentaRealizada.setGrosorBordeFilas(0);
        jTabVentaRealizada.setMultipleSeleccion(false);
        jTabVentaRealizada.setRowHeight(32);
        jTabVentaRealizada.getTableHeader().setResizingAllowed(false);
        jTabVentaRealizada.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTabVentaRealizada);

        jButVerVenta.setBackground(new java.awt.Color(0, 153, 255));
        jButVerVenta.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButVerVenta.setForeground(new java.awt.Color(255, 255, 255));
        jButVerVenta.setText("Ver Venta");
        jButVerVenta.setToolTipText("Editar Persona");
        jButVerVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButVerVenta.setIconTextGap(5);
        jButVerVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButVerVentaActionPerformed(evt);
            }
        });

        jButCancelar.setBackground(new java.awt.Color(204, 0, 0));
        jButCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButCancelar.setForeground(new java.awt.Color(255, 255, 255));
        jButCancelar.setText("Cancelar Venta");
        jButCancelar.setToolTipText("Eliminar Persona");
        jButCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButCancelar.setIconTextGap(5);
        jButCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButCancelarActionPerformed(evt);
            }
        });

        jTexTexto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTexTexto.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTexTexto.setText("Texto Guía");
        jTexTexto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTexTextoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTexTextoFocusLost(evt);
            }
        });
        jTexTexto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTexTextoMouseClicked(evt);
            }
        });
        jTexTexto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTexTextoKeyReleased(evt);
            }
        });

        jButBuscar.setBackground(new java.awt.Color(0, 153, 255));
        jButBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButBuscar.setForeground(new java.awt.Color(0, 153, 255));
        jButBuscar.setText("Buscar");
        jButBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButBuscar.setOpaque(false);
        jButBuscar.setPreferredSize(null);
        jButBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButBuscarActionPerformed(evt);
            }
        });

        jLabSubtitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabSubtitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabSubtitulo.setText("Ventas del día dd/MM/aaaa al dd/MM/aaaa");
        jLabSubtitulo.setIconTextGap(8);

        jButFechas.setBackground(new java.awt.Color(0, 153, 255));
        jButFechas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButFechas.setForeground(new java.awt.Color(255, 255, 255));
        jButFechas.setText("Buscar por Fecha");
        jButFechas.setToolTipText("Añade el producto a la venta");
        jButFechas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButFechas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButFechasActionPerformed(evt);
            }
        });

        jSeparator2.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButActual.setBackground(new java.awt.Color(0, 153, 255));
        jButActual.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButActual.setForeground(new java.awt.Color(255, 255, 255));
        jButActual.setText("Ver Todas");
        jButActual.setToolTipText("Muestra una lista con los productos.");
        jButActual.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButActualActionPerformed(evt);
            }
        });

        jButReporte.setBackground(new java.awt.Color(0, 153, 255));
        jButReporte.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButReporte.setForeground(new java.awt.Color(255, 255, 255));
        jButReporte.setText("Generar Reporte");
        jButReporte.setToolTipText("Eliminar Persona");
        jButReporte.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButReporte.setIconTextGap(5);
        jButReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButReporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButVerVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButFechas, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButActual, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabMensaje, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(68, 68, 68))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTexTexto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabSubtitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTexTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButFechas, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButActual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabSubtitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButVerVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButVerVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButVerVentaActionPerformed

        int filaSeleccionada = jTabVentaRealizada.getSelectedRow();
        if (filaSeleccionada == -1) {
            JOptionPane.showMessageDialog(null, "\t  Para continuar selecciona \n    un producto de la tabla.",
                    "Opción no válida", JOptionPane.WARNING_MESSAGE);
            return;
        }

        int id_venta = (Integer) jTabVentaRealizada.getValueAt(filaSeleccionada, 0);
        DialogVerVenta.getDialogVerVenta(null, true, id_venta).setVisible(true);
    }//GEN-LAST:event_jButVerVentaActionPerformed

    private void jButCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButCancelarActionPerformed
        try {
            int filaSeleccionada = jTabVentaRealizada.getSelectedRow();
            if (filaSeleccionada == -1) {
                JOptionPane.showMessageDialog(null, "\t  Para continuar selecciona \n    un producto de la tabla.",
                        "Opción no válida", JOptionPane.WARNING_MESSAGE);
                return;
            }

            int confirmacion = JOptionPane.showConfirmDialog(this, "¿Está seguro de cancelar la venta seleccionada?",
                    "Cancelar Venta", JOptionPane.YES_NO_OPTION);
            if (confirmacion != JOptionPane.YES_OPTION) {
                return;
            }

            int id_venta = (Integer) jTabVentaRealizada.getValueAt(filaSeleccionada, 0);
            IVenta venta = RMI.getIVentaController().newInstance();
            venta.setId_venta(id_venta);
            venta.setEstado_venta("Cancelada");
            int respuesta = RMI.getIVentaController().update(venta);
            if (respuesta == IVentaController.UPDATE_EXITO) {
                JOptionPane.showMessageDialog(this, "Venta actualizada con éxito. \n",
                        "Operación exitosa", JOptionPane.INFORMATION_MESSAGE);
                refrescarTabla();
            } else if (respuesta == IVentaController.UPDATE_ID_INEXISTENTE) {
                JOptionPane.showMessageDialog(this, "Venta no encontrada. \n Es posible que la venta haya sido eliminado previamente.",
                        "Venta no encontrada.", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Operación incompleta. \n No fue posible cancelar la venta.",
                        "Operación incompleta", JOptionPane.INFORMATION_MESSAGE);
            }

        } catch (RemoteException ex) {
            Logger.getLogger(PanelProducto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jButCancelarActionPerformed

    private void jTexTextoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexTextoFocusGained
        if (jTexTexto.getText().equals(TEXTO_GUIA)) {
            jTexTexto.setText("");
        }
    }//GEN-LAST:event_jTexTextoFocusGained

    private void jTexTextoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexTextoFocusLost
        if (jTexTexto.getText().equals("")) {
            jTexTexto.setText(TEXTO_GUIA);
        }
    }//GEN-LAST:event_jTexTextoFocusLost

    private void jTexTextoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTexTextoMouseClicked
        if (jTexTexto.getText().equals("Ingresa el texto a buscar.")) {
            jTexTexto.setText("");
        }
    }//GEN-LAST:event_jTexTextoMouseClicked

    private void jTexTextoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexTextoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            buscarElemento();
        }
    }//GEN-LAST:event_jTexTextoKeyReleased

    private void jButBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButBuscarActionPerformed
        buscarElemento();
    }//GEN-LAST:event_jButBuscarActionPerformed

    private void jButFechasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButFechasActionPerformed
        new DialogFechas(null, true).setVisible(true);
    }//GEN-LAST:event_jButFechasActionPerformed

    private void jButActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButActualActionPerformed
        jLabSubtitulo.setVisible(false);
        refrescarTabla();
    }//GEN-LAST:event_jButActualActionPerformed

    private void jButReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButReporteActionPerformed
        DialogReporte.getDialogReporte(null, true).setVisible(true);
    }//GEN-LAST:event_jButReporteActionPerformed

    private static void refrescarTabla() {
        try {
            Vector<Vector> datos = new Vector<>();
            List<IVenta> listVenta;
            listVenta = RMI.getIVentaController().list();
            List<String> fechas = RMI.getIVentaController().getTime();
            int contador = 0;
            for (IVenta venta : listVenta) {
                Vector registro = new Vector();

                registro.add(venta.getId_venta());
                registro.add(venta.getEstado_venta());
                registro.add(venta.getMonto_venta());
                registro.add(fechas.get(contador));
                datos.add(registro);
                contador++;
            }

            Vector<String> columnas = new Vector<>();
            columnas.add("Número de Venta");
            columnas.add("Estado de la venta");
            columnas.add("Monto Total");
            columnas.add("Fecha");

            jTabVentaRealizada.setModel(new javax.swing.table.DefaultTableModel(datos, columnas));
        } catch (RemoteException ex) {
            Logger.getLogger(PanelProducto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void buscarElemento() {
        Vector<Vector> datos = new Vector<>();
        if (!jTexTexto.getText().equals(TEXTO_GUIA) && !jTexTexto.getText().equals("")) {
            try {
                int indice = Integer.parseInt(jTexTexto.getText());
                IVenta venta = RMI.getIVentaController().newInstance();
                venta = RMI.getIVentaController().findOne(indice);
                String where = "id_venta = " + indice + ";";
                List<String> fecha = RMI.getIVentaController().getTime(where);
                Vector registro = new Vector();
                registro.add(venta.getId_venta());
                registro.add(venta.getEstado_venta());
                registro.add(venta.getMonto_venta());
                registro.add(fecha.get(0));
                datos.add(registro);
                Vector<String> columnas = new Vector<>();
                columnas.add("Número de Venta");
                columnas.add("Estado de la venta");
                columnas.add("Monto Total");
                columnas.add("Fecha");

                jTabVentaRealizada.setModel(new javax.swing.table.DefaultTableModel(datos, columnas));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "El valor ingresado no es válido",
                        "Valor no válido", JOptionPane.ERROR_MESSAGE);
            } catch (RemoteException ex) {
                Logger.getLogger(PanelVentaRealizada.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            refrescarTabla();
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButActual;
    private javax.swing.JButton jButBuscar;
    private javax.swing.JButton jButCancelar;
    private javax.swing.JButton jButFechas;
    private javax.swing.JButton jButReporte;
    private javax.swing.JButton jButVerVenta;
    private javax.swing.JLabel jLabMensaje;
    private static javax.swing.JLabel jLabSubtitulo;
    private javax.swing.JLabel jLabTitulo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private static rojerusan.RSTableMetro jTabVentaRealizada;
    private static javax.swing.JTextField jTexTexto;
    // End of variables declaration//GEN-END:variables
}
