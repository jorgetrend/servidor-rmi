package InterfazCliente;

import java.awt.event.KeyEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * @author Carlos
 */
public class PanelPago extends javax.swing.JPanel {

    private final String TEXTO_GUIA = "Ingresa el monto aquí";
    private static PanelPago panel;
    private static DialogPago padre;

    private PanelPago() {
        initComponents();

    }

    public static PanelPago getPanelPago(DialogPago parent) {
        if (panel == null) {
            panel = new PanelPago();
        }
        padre = parent;
        jSpinPago.requestFocus();
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlabTotal = new javax.swing.JLabel();
        jLabTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jlabCambio = new javax.swing.JLabel();
        jButAceptar = new javax.swing.JButton();
        jSpinPago = new javax.swing.JSpinner();

        setBackground(new java.awt.Color(255, 255, 255));

        jlabTotal.setFont(new java.awt.Font("Century", 1, 48)); // NOI18N
        jlabTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlabTotal.setText("$ 00,000.00");
        jlabTotal.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabTitulo.setText("Total a Pagar");

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel4.setText("Paga con:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel6.setText("Cambio:");

        jlabCambio.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabCambio.setForeground(new java.awt.Color(0, 204, 102));
        jlabCambio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jlabCambio.setText("$ 00.00");

        jButAceptar.setBackground(new java.awt.Color(0, 153, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(255, 255, 255));
        jButAceptar.setText("Aceptar");
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        jSpinPago.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jSpinPago.setModel(new javax.swing.SpinnerNumberModel(1.0d, 0.0d, null, 1.0d));
        jSpinPago.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jSpinPagoPropertyChange(evt);
            }
        });
        jSpinPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jSpinPagoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jSpinPagoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jSpinPagoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSpinPago, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4)
                            .addComponent(jlabTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jlabCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(25, 25, 25))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlabTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinPago, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlabCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        String entradaLimpia = jlabTotal.getText().replace("$ ", "");
        double precio = Double.parseDouble(entradaLimpia);
        double pago;
        pago = Double.parseDouble("" + jSpinPago.getValue());
        if (pago >= precio) {
            padre.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "La cantidad ingresada no es válida", "Cantidad no válida", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jButAceptarActionPerformed

    private void jSpinPagoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinPagoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String entradaLimpia = jlabTotal.getText().replace("$ ", "");
            double precio = Double.parseDouble(entradaLimpia);
            double pago;
            pago = Double.parseDouble("" + jSpinPago.getValue());
            if ((pago - precio) > 0) {
                jlabCambio.setText("$ " + (pago - precio) + " MXN");
            } else {
                jlabCambio.setText("$ 00.00");
            }
        }
    }//GEN-LAST:event_jSpinPagoKeyPressed

    private void jSpinPagoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinPagoKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String entradaLimpia = jlabTotal.getText().replace("$ ", "");
            double precio = Double.parseDouble(entradaLimpia);
            double pago;
            pago = Double.parseDouble("" + jSpinPago.getValue());
            if ((pago - precio) > 0) {
                jlabCambio.setText("$ " + (pago - precio) + " MXN");
            } else {
                jlabCambio.setText("$ 00.00");
            }
        }
    }//GEN-LAST:event_jSpinPagoKeyReleased

    private void jSpinPagoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jSpinPagoPropertyChange
        String entradaLimpia = jlabTotal.getText().replace("$ ", "");
        double precio = Double.parseDouble(entradaLimpia);
        double pago;
        pago = Double.parseDouble("" + jSpinPago.getValue());
        if ((pago - precio) > 0) {
            jlabCambio.setText("$ " + (pago - precio) + " MXN");
        } else {
            jlabCambio.setText("$ 00.00");
        }
    }//GEN-LAST:event_jSpinPagoPropertyChange

    private void jSpinPagoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinPagoKeyTyped
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String entradaLimpia = jlabTotal.getText().replace("$ ", "");
            double precio = Double.parseDouble(entradaLimpia);
            double pago;
            pago = Double.parseDouble("" + jSpinPago.getValue());
            if ((pago - precio) > 0) {
                jlabCambio.setText("$ " + (pago - precio) + " MXN");
            } else {
                jlabCambio.setText("$ 00.00");
            }
        }
    }//GEN-LAST:event_jSpinPagoKeyTyped

    public JLabel getJlabTotal() {
        return jlabTotal;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JButton jButAceptar;
    private static javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private static javax.swing.JSpinner jSpinPago;
    private javax.swing.JLabel jlabCambio;
    private javax.swing.JLabel jlabTotal;
    // End of variables declaration//GEN-END:variables

}
