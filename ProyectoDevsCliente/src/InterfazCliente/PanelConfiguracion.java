package InterfazCliente;

import Interfaces.Estados;
import com.sun.glass.events.KeyEvent;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * @author Carlos
 */
public class PanelConfiguracion extends javax.swing.JPanel implements Estados {

    private static PanelConfiguracion panel;
    private final String NOMBRE = "Ej. Javier";
    private final String IP = "Ej. 187.189.91.159";
    private final Properties TABLA = new Properties();
    private final Image logo;
    private static java.awt.Frame framePadre;

    private PanelConfiguracion() {
        initComponents();
        logo = new ImageIcon(("./src/media/icono.png")).getImage();
        jLabTitulo.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_PANEL, TAMANIO_PANEL, Image.SCALE_SMOOTH)));
    }

    public static PanelConfiguracion getPanelConfiguracion(java.awt.Frame frame) {
        if (panel == null) {
            panel = new PanelConfiguracion();
        }
        framePadre = frame;
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo1 = new javax.swing.JLabel();
        jLabTitulo2 = new javax.swing.JLabel();
        jTexNombre = new javax.swing.JTextField();
        jLabTitulo3 = new javax.swing.JLabel();
        jTexIp = new javax.swing.JTextField();
        jButVenta = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Bienvenido a Devs Market");
        jLabTitulo.setIconTextGap(8);

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabTitulo1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo1.setText("Ingresa los datos presentados a continuación para continuar.");
        jLabTitulo1.setIconTextGap(8);

        jLabTitulo2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabTitulo2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo2.setText("Nombre del usuario del sistema");
        jLabTitulo2.setIconTextGap(8);

        jTexNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTexNombre.setText("Ej. Javier");
        jTexNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTexNombreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTexNombreFocusLost(evt);
            }
        });
        jTexNombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTexNombreMouseClicked(evt);
            }
        });
        jTexNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTexNombreKeyPressed(evt);
            }
        });

        jLabTitulo3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabTitulo3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo3.setText("Dirección IP del servidor");
        jLabTitulo3.setIconTextGap(8);

        jTexIp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTexIp.setText("Ej. 187.189.91.159");
        jTexIp.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTexIpFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTexIpFocusLost(evt);
            }
        });
        jTexIp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTexIpMouseClicked(evt);
            }
        });
        jTexIp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTexIpKeyPressed(evt);
            }
        });

        jButVenta.setBackground(new java.awt.Color(0, 153, 255));
        jButVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButVenta.setForeground(new java.awt.Color(255, 255, 255));
        jButVenta.setText("Aceptar");
        jButVenta.setToolTipText("Agregar Persona");
        jButVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButVenta.setIconTextGap(5);
        jButVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jTexNombre, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabTitulo1, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabTitulo2, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabTitulo3, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jTexIp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabTitulo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabTitulo1)
                .addGap(18, 18, 18)
                .addComponent(jLabTitulo2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTexNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabTitulo3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTexIp, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jButVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButVentaActionPerformed
        guardarPropiedades(TABLA);
    }//GEN-LAST:event_jButVentaActionPerformed

    private void jTexNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexNombreFocusGained
        if (jTexNombre.getText().equals(NOMBRE)) {
            jTexNombre.setText("");
        }
    }//GEN-LAST:event_jTexNombreFocusGained

    private void jTexNombreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexNombreFocusLost
        if (jTexNombre.getText().equals("")) {
            jTexNombre.setText(NOMBRE);
        }
    }//GEN-LAST:event_jTexNombreFocusLost

    private void jTexIpFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexIpFocusGained
        if (jTexIp.getText().equals(IP)) {
            jTexIp.setText("");
        }
    }//GEN-LAST:event_jTexIpFocusGained

    private void jTexIpFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexIpFocusLost
        if (jTexIp.getText().equals("")) {
            jTexIp.setText(IP);
        }
    }//GEN-LAST:event_jTexIpFocusLost

    private void jTexNombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTexNombreMouseClicked
        jTexNombre.setText("");
    }//GEN-LAST:event_jTexNombreMouseClicked

    private void jTexIpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTexIpMouseClicked
        jTexIp.setText("");
    }//GEN-LAST:event_jTexIpMouseClicked

    private void jTexNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexNombreKeyPressed
        char entrada = evt.getKeyChar();
        if (!Character.isLetter(entrada) && entrada != '.' && evt.getKeyCode() != KeyEvent.VK_BACKSPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jTexNombreKeyPressed

    private void jTexIpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexIpKeyPressed
        char entrada = evt.getKeyChar();
        if (!Character.isDigit(entrada) && entrada != '.' && evt.getKeyCode() != KeyEvent.VK_BACKSPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jTexIpKeyPressed

    private void guardarPropiedades(Properties properties) {
        if (!jTexNombre.getText().equals(NOMBRE) || !jTexIp.getText().equals(IP)) {
            TABLA.setProperty("Nombre", jTexNombre.getText());
            TABLA.setProperty("IP", jTexIp.getText());
            try {
                FileOutputStream salida = new FileOutputStream("Preferencias.dat");
                properties.store(salida, "Datos ingresados por el usuario");
                salida.close();
                framePadre.dispose();
                new FramePrincipal().setVisible(true);
            } catch (Exception ex) {
                System.err.println(ex);
                JOptionPane.showMessageDialog(null, "Un error fatal ocurrió.\n El sistema se cerrará a continuación.",
                        "¡Error de conexión con el servidor!", JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Para continuar rellena todos los campos.",
                    "¡Algún campo vacío!", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static Properties getPropiedades() {
        Properties prop = new Properties();
        InputStream is = null;

        try {
            is = new FileInputStream("./Preferencias.dat");
            prop.load(is);
        } catch (IOException e) {
            System.out.println(e.toString());
        } finally {
            return prop;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButVenta;
    private javax.swing.JLabel jLabTitulo;
    private javax.swing.JLabel jLabTitulo1;
    private javax.swing.JLabel jLabTitulo2;
    private javax.swing.JLabel jLabTitulo3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTexIp;
    private javax.swing.JTextField jTexNombre;
    // End of variables declaration//GEN-END:variables
}
