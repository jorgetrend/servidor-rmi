package InterfazCliente;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 * @author Carlos
 */
public class PanelReporte extends javax.swing.JPanel {

    private static PanelReporte panel;
    private static DialogReporte padre;

    private PanelReporte() {
        initComponents();
    }

    public static PanelReporte getPanelReporte(DialogReporte parent) {
        if (panel == null) {
            panel = new PanelReporte();
        }
        padre = parent;
        rellenarCampos();
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabVentas = new javax.swing.JLabel();
        jLabCanceladas = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabMonto = new javax.swing.JLabel();
        jButAceptar = new javax.swing.JButton();
        jButGuardar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Reporte Generado");
        jLabTitulo.setIconTextGap(8);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Ventas Realizadas");
        jLabel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabVentas.setFont(new java.awt.Font("Tahoma", 1, 32)); // NOI18N
        jLabVentas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabVentas.setText("1200");
        jLabVentas.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 1, 1, new java.awt.Color(0, 0, 0)));

        jLabCanceladas.setFont(new java.awt.Font("Tahoma", 1, 32)); // NOI18N
        jLabCanceladas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabCanceladas.setText("1200");
        jLabCanceladas.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 1, 1, new java.awt.Color(0, 0, 0)));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Ventas Canceladas");
        jLabel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Monto Vendido");
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabMonto.setFont(new java.awt.Font("Tahoma", 1, 32)); // NOI18N
        jLabMonto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabMonto.setText("1200");
        jLabMonto.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 1, 1, new java.awt.Color(0, 0, 0)));

        jButAceptar.setBackground(new java.awt.Color(255, 255, 255));
        jButAceptar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButAceptar.setForeground(new java.awt.Color(0, 153, 255));
        jButAceptar.setText("Aceptar");
        jButAceptar.setToolTipText("");
        jButAceptar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 255)));
        jButAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButAceptar.setIconTextGap(5);
        jButAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAceptarActionPerformed(evt);
            }
        });

        jButGuardar.setBackground(new java.awt.Color(0, 153, 255));
        jButGuardar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButGuardar.setForeground(new java.awt.Color(255, 255, 255));
        jButGuardar.setText("Guardar");
        jButGuardar.setToolTipText("Guardar Reporte");
        jButGuardar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 255)));
        jButGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButGuardar.setIconTextGap(5);
        jButGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                        .addGap(169, 169, 169))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabCanceladas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                                    .addComponent(jLabMonto, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE))))
                        .addGap(19, 19, 19))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabMonto, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabVentas, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                            .addComponent(jLabCanceladas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAceptarActionPerformed
        padre.dispose();
    }//GEN-LAST:event_jButAceptarActionPerformed

    private void jButGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButGuardarActionPerformed
        generarReporte();
    }//GEN-LAST:event_jButGuardarActionPerformed

    private void generarReporte() {
        //Crea las instancias hacia las fecha y la hora.
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat hora = new SimpleDateFormat("HH:mm:ss");
        //Crea el nombre del archivo;
        String nombre = "Reporte_de_Ventas_DevsMarket";
        try {
            FileOutputStream archivo = new FileOutputStream("./" + nombre + ".pdf");
            Document doc = new Document();
            PdfWriter.getInstance(doc, archivo);

            doc.bottomMargin();
            doc.open();
            //---Agrega los datos del encabezado del documento.
            Paragraph parrafo = new Paragraph("DEVS MARKET");
            Paragraph parrafo1 = new Paragraph("Un producto de Devs Inc");
            Paragraph parrafo3 = new Paragraph("COATZACOALCOS, VERACRUZ");
            parrafo.setAlignment(1);
            parrafo1.setAlignment(1);
            parrafo3.setAlignment(1);

            //            ---Realiza lo necesario para la imagen.
            Image imagen = Image.getInstance("./src/media/shopping-cart.png");
            imagen.setAlignment(Element.ALIGN_CENTER);
            //Para escalar la imagen.
            imagen.scalePercent(10f);
            doc.add(imagen);

            doc.add(parrafo);
            doc.add(parrafo1);
            doc.add(parrafo3);
            //Para agregar un parrafo o línea.

            doc.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
            doc.add(new Paragraph("\n"));
            //Agrega el cuerpo del documento.

            if (PanelVentaRealizada.getjLabSubtitulo().isVisible()) {
                Paragraph titulo = new Paragraph(PanelVentaRealizada.getjLabSubtitulo().getText());
                titulo.setAlignment(Paragraph.ALIGN_CENTER);
                doc.add(new Paragraph(titulo));
                doc.add(new Paragraph("\n"));
            } else {
                Paragraph titulo = new Paragraph("Reporte de General de Ventas");
                titulo.setAlignment(Paragraph.ALIGN_CENTER);
                doc.add(titulo);
                doc.add(new Paragraph("\n"));
            }

            //Título de la tabla.
            PdfPCell celdaTitulo = new PdfPCell(new Paragraph("Transacciones Totales\n"));
            celdaTitulo.setColspan(4);
            celdaTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            celdaTitulo.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Instancia las tablas y las celdas.
            PdfPTable tabla = new PdfPTable(4);
            PdfPCell celda1 = new PdfPCell(new Paragraph(" Tipo de Transacción"));
            PdfPCell celda2 = new PdfPCell(new Paragraph(" Número de Transacciones"));

            PdfPCell celda5 = new PdfPCell(new Paragraph(" Ventas Realizadas"));
            PdfPCell celda6 = new PdfPCell(new Paragraph(jLabVentas.getText()));

            PdfPCell celda7 = new PdfPCell(new Paragraph(" Ventas Canceladas"));
            PdfPCell celda8 = new PdfPCell(new Paragraph(jLabCanceladas.getText()));

            //Posiciona el texto en la mitad de la celda.
            celda1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda1.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda1.setColspan(2);
            celda2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda2.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda2.setColspan(2);

            celda5.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda5.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda5.setColspan(2);
            celda6.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda6.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda6.setColspan(2);

            celda7.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda7.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda7.setColspan(2);
            celda8.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda8.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda8.setColspan(2);

            celdaTitulo.setFixedHeight(45f);
            celda1.setFixedHeight(40f);
            celda2.setFixedHeight(40f);
            celda5.setFixedHeight(40f);
            celda7.setFixedHeight(40f);

            //Agrega las celdas a la tabla.
            tabla.addCell(celdaTitulo);
            tabla.addCell(celda1);
            tabla.addCell(celda2);

            tabla.addCell(celda5);
            tabla.addCell(celda6);

            tabla.addCell(celda7);
            tabla.addCell(celda8);

            //Parte de totales de la tabla.
            PdfPCell celdaTotales = new PdfPCell(new Paragraph("Monto Vendido: "));
            celdaTotales.setColspan(2);
            celdaTotales.setHorizontalAlignment(Element.ALIGN_CENTER);
            celdaTotales.setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell celdaMonto = new PdfPCell(new Paragraph(jLabMonto.getText()));
            celdaMonto.setColspan(2);
            celdaMonto.setHorizontalAlignment(Element.ALIGN_CENTER);
            celdaMonto.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Determina la altura de las celdas de la tabla.
            celdaTotales.setFixedHeight(45f);

            tabla.addCell(celdaTotales);
            tabla.addCell(celdaMonto);

            //Ajusta el tamaño de las celdas.
            float[] medidaCeldas = {2.2f, 1.25f, 1.0f, 1.3f};
            tabla.setWidths(medidaCeldas);

            //Agrega la tabla al documento.
            doc.add(tabla);

            //Agrega el pie de la página 
            doc.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
            Paragraph Parrafo7 = new Paragraph("Documento generado el: " + dateFormat.format(date) + " a las: " + hora.format(date) + " horas.");
            Parrafo7.setAlignment(1);
            doc.add(Parrafo7);

            Paragraph Parrafo8 = new Paragraph("Documento generado por "+PanelConfiguracion.getPropiedades().getProperty("Nombre"));
            Parrafo8.setAlignment(Paragraph.ALIGN_CENTER);
            doc.add(Parrafo8);
            
            doc.close();
            //Abre el archivo en el programa predeterminado para abrir PDF´s.
            try {
                File path = new File("./" + nombre + ".pdf");
                Desktop.getDesktop().open(path);
            } catch (IOException ex) {
                ex.printStackTrace();
                System.err.println("Error al abrir el archivo");
            }//End try-catch
        } catch (DocumentException ex) {
            JOptionPane.showMessageDialog(null, "Ocurrió un error al abrir el archivo", "Error", JOptionPane.ERROR_MESSAGE);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Ocurrió un error al abrir el archivo", "Error", JOptionPane.ERROR_MESSAGE);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocurrió un error al abrir el archivo", "Error", JOptionPane.ERROR_MESSAGE);

        }//End try-catch
    }

    private static void rellenarCampos() {
        int realizada = 0;
        int cancelada = 0;
        double cantidad = 0;
        try {
            JTable jTabProducto = PanelVentaRealizada.getjTabVentaRealizada();
            TableModel tableModel = jTabProducto.getModel();
            System.err.println(tableModel.getRowCount());
            String estado = "";
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                estado = "" + tableModel.getValueAt(i, 1);
                if (estado.equals("Realizada")) {
                    realizada++;
                    cantidad += Double.parseDouble("" + tableModel.getValueAt(i, 2));
                } else {
                    cancelada++;
                }
            }
            jLabVentas.setText("" + realizada);
            jLabCanceladas.setText("" + cancelada);
            jLabMonto.setText("$ " + cantidad);
        } catch (NullPointerException ex) {
            System.err.println(ex.getCause());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButAceptar;
    private javax.swing.JButton jButGuardar;
    private static javax.swing.JLabel jLabCanceladas;
    private static javax.swing.JLabel jLabMonto;
    private javax.swing.JLabel jLabTitulo;
    private static javax.swing.JLabel jLabVentas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables

}
