package InterfazCliente;

import Interfaces.Estados;
import static Interfaces.Estados.TAMANIO_PANEL;
import Interfaces.IPedido;
import Interfaces.IProducto;
import Interfaces.IVenta;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import practica01cliente.RMI;
import rojerusan.RSTableMetro;

/**
 * @author Carlos
 */
public class PanelNuevaVenta extends javax.swing.JPanel {

    private static final String TEXTO_GUIA = "Ingresa el nombre del producto para agregar.";
    private static PanelNuevaVenta panel;
    private Image logo;

    private PanelNuevaVenta() {
        initComponents();
        jTexTexto.requestFocus();
        logo = new ImageIcon(("./src/media/money.png")).getImage();
        jLabTitulo.setIcon(new ImageIcon(logo.getScaledInstance(TAMANIO_PANEL, TAMANIO_PANEL, Image.SCALE_SMOOTH)));
        limpiarTabla();
    }

    public static PanelNuevaVenta getPanelVentas() {
        if (panel == null) {
            panel = new PanelNuevaVenta();
        }
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jLabTitulo = new javax.swing.JLabel();
        jLabMensaje = new javax.swing.JLabel();
        jTexTexto = new javax.swing.JTextField();
        jButAgregar = new javax.swing.JButton();
        jButBuscar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTabProducto = new rojerusan.RSTableMetro();
        jSpinCantidad = new javax.swing.JSpinner();
        jlbTotalPagar = new javax.swing.JLabel();
        jlbTotalPagarInfo = new javax.swing.JLabel();
        jlbProductosInfo = new javax.swing.JLabel();
        jlbProductos = new javax.swing.JLabel();
        jButEliminar = new javax.swing.JButton();
        jButEditar = new javax.swing.JButton();
        jButCancelar = new javax.swing.JButton();
        jButVenta = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));

        jLabTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabTitulo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabTitulo.setText("Nueva Venta");
        jLabTitulo.setIconTextGap(8);

        jLabMensaje.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabMensaje.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabMensaje.setText("Realiza una nueva venta con los productos registrados");
        jLabMensaje.setIconTextGap(8);

        jTexTexto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTexTexto.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTexTexto.setText("Ingresa el nombre del producto para agregar.");
        jTexTexto.setMargin(new java.awt.Insets(2, 8, 2, 2));
        jTexTexto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTexTextoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTexTextoFocusLost(evt);
            }
        });
        jTexTexto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTexTextoMouseClicked(evt);
            }
        });
        jTexTexto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTexTextoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTexTextoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTexTextoKeyTyped(evt);
            }
        });

        jButAgregar.setBackground(new java.awt.Color(0, 153, 255));
        jButAgregar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButAgregar.setForeground(new java.awt.Color(255, 255, 255));
        jButAgregar.setText("Agregar Producto");
        jButAgregar.setToolTipText("Añade el producto a la venta");
        jButAgregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButAgregarActionPerformed(evt);
            }
        });

        jButBuscar.setBackground(new java.awt.Color(0, 153, 255));
        jButBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButBuscar.setForeground(new java.awt.Color(255, 255, 255));
        jButBuscar.setText("Buscar Producto");
        jButBuscar.setToolTipText("Muestra una lista con los productos.");
        jButBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButBuscarActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jTabProducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTabProducto.setAltoHead(40);
        jTabProducto.setColorBackgoundHead(new java.awt.Color(0, 0, 0));
        jTabProducto.setColorFilasForeground1(new java.awt.Color(0, 0, 0));
        jTabProducto.setColorFilasForeground2(new java.awt.Color(0, 0, 0));
        jTabProducto.setFocusable(false);
        jTabProducto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabProducto.setFuenteFilas(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabProducto.setFuenteFilasSelect(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTabProducto.setGrosorBordeFilas(0);
        jTabProducto.setMultipleSeleccion(false);
        jTabProducto.setRowHeight(32);
        jTabProducto.getTableHeader().setResizingAllowed(false);
        jTabProducto.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTabProducto);

        jSpinCantidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jSpinCantidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        jSpinCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jSpinCantidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jSpinCantidadKeyReleased(evt);
            }
        });

        jlbTotalPagar.setBackground(new java.awt.Color(255, 255, 255));
        jlbTotalPagar.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jlbTotalPagar.setForeground(new java.awt.Color(0, 204, 102));
        jlbTotalPagar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlbTotalPagar.setText("$ 00,000.00 MXN");
        jlbTotalPagar.setOpaque(true);

        jlbTotalPagarInfo.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jlbTotalPagarInfo.setText("Total a Pagar");

        jlbProductosInfo.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jlbProductosInfo.setText("Productos Agregados");

        jlbProductos.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jlbProductos.setForeground(new java.awt.Color(0, 153, 255));
        jlbProductos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlbProductos.setText("00");

        jButEliminar.setBackground(new java.awt.Color(255, 255, 255));
        jButEliminar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButEliminar.setForeground(new java.awt.Color(204, 0, 0));
        jButEliminar.setText("Eliminar Producto");
        jButEliminar.setToolTipText("Elimina un producto de la venta actual.");
        jButEliminar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 0, 0)));
        jButEliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButEliminarActionPerformed(evt);
            }
        });

        jButEditar.setBackground(new java.awt.Color(255, 255, 255));
        jButEditar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButEditar.setForeground(new java.awt.Color(0, 153, 255));
        jButEditar.setText("Editar Producto");
        jButEditar.setToolTipText("Edita la cantidad de un producto añadido");
        jButEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButEditarActionPerformed(evt);
            }
        });

        jButCancelar.setBackground(new java.awt.Color(204, 0, 0));
        jButCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButCancelar.setForeground(new java.awt.Color(255, 255, 255));
        jButCancelar.setText("Cancelar Venta");
        jButCancelar.setToolTipText("Cancela la venta actual.");
        jButCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButCancelarActionPerformed(evt);
            }
        });

        jButVenta.setBackground(new java.awt.Color(0, 204, 102));
        jButVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButVenta.setForeground(new java.awt.Color(255, 255, 255));
        jButVenta.setText("Realizar Venta");
        jButVenta.setToolTipText("Registra la venta actual.");
        jButVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jlbProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlbProductosInfo))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jlbTotalPagarInfo)
                                        .addGap(59, 59, 59))
                                    .addComponent(jlbTotalPagar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabMensaje, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(31, 31, 31)
                                        .addComponent(jButEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                        .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTexTexto)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSpinCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(33, 33, 33)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTexTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2)
                    .addComponent(jSpinCantidad))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlbTotalPagarInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlbProductosInfo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlbProductos)
                    .addComponent(jlbTotalPagar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public RSTableMetro getjTabProducto() {
        return jTabProducto;
    }


    private void jTexTextoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexTextoFocusGained
        if (jTexTexto.getText().equals(TEXTO_GUIA)) {
            jTexTexto.setText("");
        }
    }//GEN-LAST:event_jTexTextoFocusGained

    private void jTexTextoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTexTextoFocusLost
        if (jTexTexto.getText().equals("")) {
            jTexTexto.setText(TEXTO_GUIA);
        }
    }//GEN-LAST:event_jTexTextoFocusLost

    private void jTexTextoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTexTextoMouseClicked
        if (jTexTexto.getText().equals(TEXTO_GUIA)) {
            jTexTexto.setText("");
        }
    }//GEN-LAST:event_jTexTextoMouseClicked

    private void jTexTextoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexTextoKeyReleased
        char entrada = evt.getKeyChar();
        if (!Character.isLetterOrDigit(entrada) && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && evt.getKeyCode() != KeyEvent.VK_SPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            buscarProducto();
        }
    }//GEN-LAST:event_jTexTextoKeyReleased

    private void jButAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButAgregarActionPerformed
        buscarProducto();
    }//GEN-LAST:event_jButAgregarActionPerformed

    private void jButBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButBuscarActionPerformed
        new DialogBusqueda(null, true).setVisible(true);
        actualizarPrecio();
        actualizarProductos();
        jTexTexto.setText(TEXTO_GUIA);
        jTexTexto.requestFocus();
        jSpinCantidad.setValue(1);
    }//GEN-LAST:event_jButBuscarActionPerformed

    private void jButEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButEliminarActionPerformed
        int filaSeleccionada = jTabProducto.getSelectedRow();
        if (filaSeleccionada == -1) {
            JOptionPane.showMessageDialog(null, "\t  Para continuar selecciona \n    un producto de la tabla.",
                    "Opción no válida", JOptionPane.WARNING_MESSAGE);
            return;
        }

        int confirmacion = JOptionPane.showConfirmDialog(this, "¿Está seguro de  eliminar el \n    producto de la venta?",
                "Eliminar Producto", JOptionPane.YES_NO_OPTION);
        if (confirmacion == JOptionPane.YES_OPTION) {
            DefaultTableModel tableModel = (DefaultTableModel) jTabProducto.getModel();
            tableModel.removeRow(filaSeleccionada);
            actualizarPrecio();
            actualizarProductos();
        } else {
            return;
        }


    }//GEN-LAST:event_jButEliminarActionPerformed

    private void jButEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButEditarActionPerformed
        int filaSeleccionada = jTabProducto.getSelectedRow();
        if (filaSeleccionada == -1) {
            JOptionPane.showMessageDialog(null, "\t  Para continuar selecciona \n    un producto de la tabla.",
                    "Opción no válida", JOptionPane.WARNING_MESSAGE);
            return;
        }

        DefaultTableModel tableModel = (DefaultTableModel) jTabProducto.getModel();
        String valor = JOptionPane.showInputDialog(null,
                "Ingresa la nueva cantidad del producto:\n  --> " + tableModel.getValueAt(filaSeleccionada, 1),
                (int) tableModel.getValueAt(filaSeleccionada, 2));
        try {
            if (Integer.parseInt(valor) > 0) {
                double monto = Double.parseDouble("" + (Integer.parseInt(valor) * ((Double) tableModel.getValueAt(filaSeleccionada, 3))));
                tableModel.setValueAt(Integer.parseInt(valor), filaSeleccionada, 2);
                tableModel.setValueAt(monto, filaSeleccionada, 4);
                actualizarPrecio();
                actualizarProductos();
            } else {
                JOptionPane.showMessageDialog(null, "El valor ingresado no es válido",
                        "Valor no válido", JOptionPane.ERROR_MESSAGE);
            }
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, "La cantidad ingresada no es un número válido", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }


    }//GEN-LAST:event_jButEditarActionPerformed

    private void jButCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButCancelarActionPerformed
        if (jTabProducto.getRowCount() > 0) {
            int respuesta = JOptionPane.showConfirmDialog(null, "¿Deseas cancelar la venta actual?",
                    "¿Estás seguro?", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_OPTION) {
                limpiarTabla();
            }
        }
    }//GEN-LAST:event_jButCancelarActionPerformed

    private void jButVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButVentaActionPerformed
        if (jTabProducto.getRowCount() > 0) {
            PanelPago.getPanelPago(null).getJlabTotal().setText("$ " + jlbTotalPagar.getText().replace("$", "").replace("MXN", "").trim());
            new DialogPago(null, true).setVisible(true);
            insertarVenta();
        } else {
            JOptionPane.showMessageDialog(null, "Agrega un producto para continuar.",
                    "Venta vacía", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButVentaActionPerformed

    private void jTexTextoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexTextoKeyPressed
        char entrada = evt.getKeyChar();
        if (!Character.isLetterOrDigit(entrada) && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && evt.getKeyCode() != KeyEvent.VK_SPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jTexTextoKeyPressed

    private void jTexTextoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTexTextoKeyTyped
        char entrada = evt.getKeyChar();
        if (!Character.isLetterOrDigit(entrada) && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && evt.getKeyCode() != KeyEvent.VK_SPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jTexTextoKeyTyped

    private void jSpinCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinCantidadKeyPressed
        char entrada = evt.getKeyChar();
        if (!Character.isDigit(entrada) && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && evt.getKeyCode() != KeyEvent.VK_SPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jSpinCantidadKeyPressed

    private void jSpinCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinCantidadKeyReleased
        char entrada = evt.getKeyChar();
        if (!Character.isDigit(entrada) && evt.getKeyCode() != KeyEvent.VK_BACK_SPACE
                && evt.getKeyCode() != KeyEvent.VK_SPACE && evt.getKeyCode() != KeyEvent.VK_DELETE) {
            evt.consume();
        }
    }//GEN-LAST:event_jSpinCantidadKeyReleased

    private void buscarProducto() {
        try {
            String texto = jTexTexto.getText();
            Vector<Vector> datos = new Vector<>();
            int cantidad = Integer.parseInt(jSpinCantidad.getValue().toString());
            if (!texto.equals(TEXTO_GUIA)) {

                IProducto producto = RMI.getIProductoController().newInstance();
                producto.setNombre_producto(texto);
                List<IProducto> listProductos = RMI.getIProductoController().find(producto);
                if (listProductos.size() <= 0) {
                    JOptionPane.showMessageDialog(null, "El producto ingresado no está registrado",
                            "Error el producto no existe", JOptionPane.WARNING_MESSAGE);
                }

                Vector registro = new Vector();

                for (IProducto productos : listProductos) {
                    registro.add(productos.getId_producto());
                    registro.add(productos.getNombre_producto());
                    registro.add(cantidad);
                    registro.add(productos.getPrecio_producto());
                    registro.add(cantidad * productos.getPrecio_producto());
                    datos.add(registro);
                }

                Vector<String> columnas = new Vector<>();
                columnas.add("Id Producto");
                columnas.add("Nombre del producto");
                columnas.add("Cantidad");
                columnas.add("Precio Unitario");
                columnas.add("Monto total");

                if (jTabProducto.getRowCount() <= 0) {
                    jTabProducto.setModel(new javax.swing.table.DefaultTableModel(datos, columnas));
                    jTabProducto.getColumnModel().getColumn(0).setMaxWidth(0);
                    jTabProducto.getColumnModel().getColumn(0).setMinWidth(0);
                    jTabProducto.getColumnModel().getColumn(0).setPreferredWidth(0);
                    jTabProducto.getColumnModel().getColumn(0).setResizable(false);
                } else {
                    try {
                        DefaultTableModel tableModel = (DefaultTableModel) jTabProducto.getModel();
                        tableModel.addRow(datos.firstElement());
                    } catch (NullPointerException ex) {
                        //Guardar errores.
                    }
                }
                actualizarPrecio();
                actualizarProductos();
                jTexTexto.setText(TEXTO_GUIA);
                jTexTexto.requestFocus();
                jSpinCantidad.setValue(1);

            } else {
                JOptionPane.showMessageDialog(null, "\t  Ingresa un producto para continuar.",
                        "Opción no válida", JOptionPane.WARNING_MESSAGE);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(PanelNuevaVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizarPrecio() {
        int totalFilas = jTabProducto.getRowCount();
        double temp;
        double total = 0;
        if (totalFilas > 0) {
            for (int i = 0; i < totalFilas; i++) {
                temp = Double.parseDouble("" + jTabProducto.getValueAt(i, 4));
                total += temp;
            }
            jlbTotalPagar.setText("$" + total + " MXN");
        } else {
            jlbTotalPagar.setText("$ 00.00 MXN");
        }
    }

    private void actualizarProductos() {
        int totalFilas = jTabProducto.getRowCount();
        double temp = 0;
        double total = 0;
        if (totalFilas > 0) {
            for (int i = 0; i < totalFilas; i++) {
                temp = Double.parseDouble("" + jTabProducto.getValueAt(i, 2));
                total += temp;
            }
            jlbProductos.setText("" + (int) total);
        } else {
            jlbProductos.setText("0");
        }
    }

    private void insertarVenta() {
        try {
            IVenta venta = RMI.getIVentaController().newInstance();
            venta.setMonto_venta(Double.parseDouble(jlbTotalPagar.getText().replace("$", "").replace(" MXN", "")));
            //Insertar datos
            int respuesta = RMI.getIVentaController().add(venta);
            if (respuesta == Estados.ADD_SIN_EXITO) {
                System.out.println(respuesta);
                JOptionPane.showMessageDialog(
                        this,
                        "No se pudo realizar la venta",
                        "Acción no completada",
                        JOptionPane.ERROR_MESSAGE
                );
            } else {
                insertarPedido();
            }
        } catch (RemoteException ex) {
            Logger.getLogger(PanelAgregarProducto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void insertarPedido() {
        try {
            IPedido pedido = RMI.getIPedidoController().newInstance();
            int id_venta = RMI.getIPedidoController().findMax();
            for (int i = 0; i < jTabProducto.getRowCount(); i++) {
                pedido.setId_producto((int) jTabProducto.getValueAt(i, 0));
                pedido.setId_venta(id_venta);
                pedido.setCantidad_producto((int) jTabProducto.getValueAt(i, 2));
                pedido.setPrecio_unitario((double) jTabProducto.getValueAt(i, 3));
                pedido.setPrecio_total((double) jTabProducto.getValueAt(i, 4));

                //Insertar datos
                int respuesta = RMI.getIPedidoController().add(pedido);
                if (respuesta == Estados.ADD_SIN_EXITO) {
                    System.out.println(respuesta);
                    JOptionPane.showMessageDialog(
                            this,
                            "No se pudo realizar el registro de la venta",
                            "Acción no completada",
                            JOptionPane.ERROR_MESSAGE
                    );
                }
            }

            JOptionPane.showMessageDialog(this,
                    "Compra realizada con éxito.",
                    "Finalizado", JOptionPane.INFORMATION_MESSAGE);
            limpiarTabla();
        } catch (RemoteException ex) {
            Logger.getLogger(PanelAgregarProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void limpiarTabla() {
        Vector<String> columnas = new Vector<>();
        columnas.add("Id Producto");
        columnas.add("Nombre del producto");
        columnas.add("Cantidad");
        columnas.add("Precio Unitario");
        columnas.add("Monto total");
        jTabProducto.setModel(new javax.swing.table.DefaultTableModel(null, columnas));
        jTabProducto.getColumnModel().getColumn(0).setMaxWidth(0);
        jTabProducto.getColumnModel().getColumn(0).setMinWidth(0);
        jTabProducto.getColumnModel().getColumn(0).setPreferredWidth(0);
        jTabProducto.getColumnModel().getColumn(0).setResizable(false);
        jlbProductos.setText("0");
        jlbTotalPagar.setText("$00.00");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButAgregar;
    private javax.swing.JButton jButBuscar;
    private javax.swing.JButton jButCancelar;
    private javax.swing.JButton jButEditar;
    private javax.swing.JButton jButEliminar;
    private javax.swing.JButton jButVenta;
    private javax.swing.JLabel jLabMensaje;
    private javax.swing.JLabel jLabTitulo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSpinner jSpinCantidad;
    private rojerusan.RSTableMetro jTabProducto;
    private javax.swing.JTextField jTexTexto;
    private javax.swing.JLabel jlbProductos;
    private javax.swing.JLabel jlbProductosInfo;
    private javax.swing.JLabel jlbTotalPagar;
    private javax.swing.JLabel jlbTotalPagarInfo;
    // End of variables declaration//GEN-END:variables

}
